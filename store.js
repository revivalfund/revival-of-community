import {useMemo} from 'react'
import {createStore, applyMiddleware, Store} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import {createWrapper, Context, HYDRATE} from 'next-redux-wrapper';
import {getCookie, setCookie} from "./libs/useCookie";
const LANG = 'LANG';
let store;

const initialState = {
    border: true,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'BORDER_VISIBLE':
            return {
                ...state,
                border: action.border,
            }

        default:
            return state
    }
}

function initStore(preloadedState = initialState) {
    preloadedState.lang = getCookie(LANG)

    return createStore(
        reducer,
        preloadedState,
        composeWithDevTools(applyMiddleware())
    )
}

export const initializeStore = (preloadedState) => {
    let _store = store ?? initStore(preloadedState)

    if (preloadedState && store) {
        _store = initStore({
            ...store.getState(),
            ...preloadedState,
        })
        store = undefined
    }

    if (typeof window === 'undefined') return _store

    if (!store) store = _store

    return _store
}

export function useStore(initialState) {
    const store = useMemo(() => initializeStore(initialState), [initialState])
    return store
}

const makeStore = context => createStore(reducer);
export const wrapper = createWrapper(makeStore, {debug: false});
