module.exports = {
    i18n: {
        locales: ['ua', 'en'],
        defaultLocale: 'ua',
        localeDetection: false
    },
};