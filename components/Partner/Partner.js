import Image from "next/image";
import Link from 'next/link';

import style from './Partner.module.scss';


export const Partner = ({img, type, title, projects}) => {
    return (
        <div className={style.partner}>
            <div className={style.partner_name}>
                {/*<h3>{title}</h3>*/}
                <div className={style.partner_logo + ' ' + (type === 'square' ? style.square : '')}>
                    <Image
                        src={img}
                        layout='fill'
                        alt='partner'
                        objectFit='contain'
                        loading='eager'
                        placeholder="blur"
                    />
                </div>
            </div>

            <ul className={style.partner_projects}>
                {projects?.map((project, idx) =>
                    <li key={idx}>
                        {project?.link
                            ? <Link href={project.link}>{project?.text}</Link>
                            : <span>{project?.text}</span>
                        }
                    </li>
                )}
            </ul>
        </div>
    )
}
