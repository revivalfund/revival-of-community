import Link from "next/link";
import Image from "next/image";

export default function BlogPreview({slug, image, title, description, date}) {
    return (
        <Link href={slug}>
            <a className='blogPreview__item'>
                <div className='blogPreview__image'>
                    <Image
                        src={image}
                        layout='fill'
                        alt='blog-image'
                        objectFit='cover'
                        loading='eager'
                        placeholder="blur"
                        objectPosition='top'
                    />
                </div>

                <div className='blogPreview__text'>
                    <h2>{title}</h2>
                    <p>{description}</p>
                    <span>{date}</span>
                </div>
            </a>
        </Link>
    )
}