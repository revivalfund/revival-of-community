import {Swiper, SwiperSlide} from "swiper/react";
import {Autoplay} from "swiper";
import Image from "next/image";
import {useState} from "react";
import "swiper/css";

export default function BlogSlider({images}) {
    const [navSwiper, setNavSwiper] = useState(null);

    return (
        <div className='blog__slider'>
            <Swiper
                onSwiper={setNavSwiper}
                slidesPerView={1}
                breakpoints={{
                    640: {
                        slidesPerView: 2,
                    },
                    // when window width is >= 768px
                    1024: {
                        slidesPerView: 3,
                    },
                }}
                spaceBetween={20}
                autoplay={{
                    delay: 2500,
                    disableOnInteraction: false,
                }}
                modules={[Autoplay]}
            >
                {images.map((el, i) =>
                    <SwiperSlide className='blog__slide'>
                        <Image
                            src={el.img}
                            layout='fill'
                            alt='reports'
                            objectFit='cover'
                            loading='eager'
                            placeholder="blur"
                        />
                    </SwiperSlide>
                )}
            </Swiper>
        </div>
    )
}