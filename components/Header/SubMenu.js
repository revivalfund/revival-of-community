import Link from "next/link";
import { useTranslation } from "next-i18next";
import {useLayoutEffect, useState} from "react";

import style from "./Header.module.scss";

export const AboutList = ({content}) => {
    const { t } = useTranslation('common')
    const { title, links} = content

    const [layoutReady, setLayoutReady] = useState(false)
    useLayoutEffect(() => {
        const timeout = setTimeout(() => {
            setLayoutReady(true)
        }, 100)

        return () => clearTimeout(timeout)
    }, [])

    return <>
        <a>{title}</a>

        {layoutReady && <ul className={style.hide_menu}>
            {links.map((el, i) => {
                    if (!el.name || !el.href) return

                    return (
                        <li key={i}>
                            <Link href={el?.href}>
                                <a>{el?.name}</a>
                            </Link>
                        </li>
                    )
                }
            )}
        </ul>}
    </>
}