import Link from 'next/link';
import { AboutList } from './SubMenu';
import { useTranslation } from "next-i18next";
import {useEffect, useRef, useState} from "react";
import { useMediaQuery } from "../../libs/useMediaQuery";
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import Image from "next/image";

import style from './Header.module.scss';

import logo_ua from '../../public/images/logo_ua.png';
import logo_eu from '../../public/images/logo_eu.png';



const Header = () => {
    const { t } = useTranslation('common')
    const router = useRouter();

    const [activeMenu, setActiveMenu] = useState(false);
    const breakpoint = useMediaQuery(655);

    const targetRef = useRef()

    useEffect(()=> {
        activeMenu ? disableBodyScroll(targetRef) : enableBodyScroll(targetRef);
        return () => {
            clearAllBodyScrollLocks();
        }
    }, [activeMenu])


    const aboutMenu = {
        title: t("header.about"),
        links: [
            {
                href: '/about/history',
                name: t("header.submenu.history")
            },
            {
                href: '/about/history#goals',
                name: t("header.submenu.goals")
            },
            {
                href: '/about/fund-documents',
                name: t("header.submenu.documents")
            },
            {
                href: '/projects',
                name: t("header.submenu.projects")
            },
            {
                href: '/about/report',
                name: t("header.submenu.reports")
            },
            {
                href: '/about/purchase',
                name: router.locale === 'ua' ? t("header.submenu.purchase") : ''
            },
            {
                href: '/team',
                name: t("header.submenu.team")
            },
            {
                href: '/about/partners',
                name: t("header.submenu.partners")
            },
        ]
    }
    const projectMenu = {
        title: t("header.activities"),
        links: [
            {
                href: '/activities/psychosocial',
                name: t("main-page.activities.psychosocial")
            },
            {
                href: '/activities/restoration',
                name: t("main-page.activities.restoration")
            },
            {
                href: '/activities/children',
                name: t("main-page.activities.children")
            },
            {
                href: '/activities/elderly',
                name: t("main-page.activities.elderly")
            },

            // {
            //     href: '/activities/animals',
            //     name: t("main-page.activities.animal")
            // },
        ]
    }


    return (
        // <header className={style.header + ' ' + (!breakpoint && (border ? style.up : style.down))}>
        //     <div className={style.container + ' ' + (!breakpoint && (border ? style.up : style.down))}>
        <header className={style.header}>
            <div className={style.container}>
                <Link href='/'>
                    <a className={style.logo + ' ' + (activeMenu && style.active)}>
                        <Image
                            src={router.locale === 'ua' ? logo_ua : logo_eu}
                            layout='fill'
                            alt='logo'
                            objectFit='cover'
                        />
                    </a>
                </Link>


                {!breakpoint &&
                    <nav>
                        <ul className={style.menu}>
                            <li>
                                <Link href="/">
                                    <a>{t("header.main")}</a>
                                </Link>
                            </li>
                            <li className={style.about_list}>
                                <AboutList content={aboutMenu}/>
                            </li>
                            <li className={style.about_list}>
                                <AboutList content={projectMenu}/>
                            </li>
                            <li>
                                <Link href="/news">
                                    <a>{t("header.news")}</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/help">
                                    <a>{t("header.want-help")}</a>
                                </Link>
                            </li>
                        </ul>
                    </nav>
                }

                {breakpoint &&
                    <nav className={style.mobile_menu} ref={targetRef}>
                        <div className={style.mobile_menu_wrap + ' ' + (activeMenu && style.active)}>
                            <ul>
                                <li>
                                    <Link href="/">
                                        <a>{t("header.main")}</a>
                                    </Link>
                                </li>
                                <li className={style.about_list}>
                                    <AboutList content={aboutMenu}/>
                                </li>
                                <li className={style.about_list}>
                                    <AboutList content={projectMenu}/>
                                </li>
                                <li>
                                    <Link href="/news">
                                        <a>{t("header.news")}</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/help">
                                        <a>{t("header.want-help")}</a>
                                    </Link>
                                </li>
                            </ul>
                        </div>

                        <div
                            className={style.mobile_menu_background+ ' ' + (activeMenu && style.active)}
                            onClick={()=>{setActiveMenu(!activeMenu)}}
                        />

                        <div
                            className={style.burger + ' ' + (activeMenu && style.active)}
                            onClick={()=>{setActiveMenu(!activeMenu)}}
                        >
                            <span/>
                            <span/>
                            <span/>
                        </div>
                    </nav>
                }

                <div className={style.lang}>
                    <div className={style.lang_wrap}>
                        <Link
                            href={router.asPath}
                            locale={router.locale === 'ua' ? 'en' : 'ua'}
                        >
                            <div className={style.lang_type + ' ' + style.second}>
                                {router.locales.filter(lang => lang !== router.locale)}
                            </div>
                        </Link>
                        <div className={style.lang_type}>
                            {router.locale}
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
}

export {Header};
