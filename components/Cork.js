import Image from 'next/image';

import error from "../public/images/error.jpg";
import {useTranslation} from "next-i18next";

const Cork = () => {
    const { t } = useTranslation('common')

    return <>
        <div className='cork'>
            <h2>{t("cork")}</h2>

            <div className='cork-img'>
                <Image
                    src={error}
                    layout='fill'
                    alt='error'
                    objectFit='cover'
                />
            </div>
        </div>
    </>

}

export default Cork;