import Link from "next/link";
import Image from "next/image";

export const NewsCard = ({slug, image, title, date}) => {
    return (
        <Link href={slug}>
            <a className='news__item'>
                <div className='news__item-image'>
                    <Image
                        src={image}
                        layout='fill'
                        alt='blog-image'
                        objectFit='cover'
                        loading='eager'
                        placeholder="blur"
                    />
                </div>

                <div className='news__item-text'>
                    <p className='news__item-text__title'>
                        <h2>{title}</h2>
                    </p>
                    <span className='news__item-text__date'>{date}</span>
                </div>
            </a>
        </Link>
    )
}