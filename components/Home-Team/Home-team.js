import { Swiper, SwiperSlide } from 'swiper/react';
import { Parallax, Pagination, Navigation } from "swiper";
import { useTranslation } from "next-i18next";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import {useRef, useState} from "react";


const HomeTeam = () => {
    const { t } = useTranslation('common')

    const arr = [
        {
            img: "/images/team/Sergey.jpg",
            name: t("main-page.team.Sergey"),
            // birth: '(10 червня 1980 року народження)'
            position: `${t("main-page.team.position.founder")}, ${t("main-page.team.position.director")}`
        },
        {
            img: "/images/team/Taras.jpg",
            name: t("main-page.team.Taras"),
            // birth: '(05 липня 1978 року народження)'
            position: t("main-page.team.position.founder")
        },
        {
            img: "/images/team/Nastya.jpg",
            name: t("main-page.team.Nastya"),
            position: t("main-page.team.position.lawyer")
        },
        {
            img: "/images/team/Olga.jpg",
            name: t("main-page.team.Olga"),
            position: t("main-page.team.position.smm")
        },
        {
            img: "/images/team/Tanya.jpg",
            name: t("main-page.team.Tanya"),
            position: t("main-page.team.position.accountant")
        },
        {
            img: "/images/team/Bogdan.jpg",
            name: t("main-page.team.Bogdan"),
            position: t("main-page.team.position.volunteer")
        },
        {
            img: "/images/team/Oleksandr.jpg",
            name: t("main-page.team.Oleksandr"),
            position: t("main-page.team.position.finance")
        },
    ]

    const [navSwiper, setNavSwiper] = useState(null);

    const prevRef = useRef(null);
    const nextRef = useRef(null);
    const paginationRef = useRef(null);

    const pagination = {
        el: paginationRef.current,
        clickable: true,
        renderBullet: (index, className) => {
            return `<span class="${className}"></span>`;
        }
    };

    return (
        <div className='home-team' id='team'>
            <Swiper
                style={{
                    "--swiper-navigation-color": "#fff",
                    "--swiper-pagination-color": "#fff",
                }}
                onSwiper={setNavSwiper}
                speed={600}
                parallax={true}
                slidesPerView={3}
                centeredSlides={true}
                grabCursor={true}
                keyboard={{enabled: true}}
                pagination={pagination}
                navigation={{
                    prevEl: prevRef.current,
                    nextEl: nextRef.current,
                }}
                breakpoints={{
                    0: {
                        slidesPerView: 1,
                    },
                    500: {
                        slidesPerView: 2,
                    },
                    768: {
                        slidesPerView: 3,
                    },
                }}
                modules={[Parallax, Pagination, Navigation]}
                className="mySwiper"
            >
                <div
                    slot="container-start"
                    className="parallax-bg"
                    data-swiper-parallax="-23%"
                />
                <div className='overlay'/>

                <>
                    {arr.map((el, i) => (
                        <SwiperSlide key={i}>
                            <div className="card">
                                <img src={el?.img} alt=""/>
                                <h3>{el?.name}</h3>
                                <p>{el?.position}</p>
                                {/*<p>{el?.birth}</p>*/}
                            </div>
                        </SwiperSlide>
                    ))}
                </>

                <div className='team-controller'>
                    <a className="team-prev arrow" ref={prevRef}><span/></a>
                    <div className="team-bullet" ref={paginationRef}/>
                    <a className="team-next arrow" ref={nextRef}><span/></a>
                </div>
            </Swiper>
        </div>
    );
}

export {HomeTeam};