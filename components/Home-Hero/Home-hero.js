import { Parallax } from 'react-parallax';
import Link from "next/link";
import {useEffect, useState, useRef, useLayoutEffect} from "react";
import { useInView } from "react-intersection-observer";
import { useDispatch } from "react-redux";
import { clearAllBodyScrollLocks, disableBodyScroll, enableBodyScroll } from "body-scroll-lock";
import { useTranslation } from "next-i18next";
import Image from "next/image";

import style from './Home-hero.module.scss';

import un from "../../public/images/partners/un.png";
import pact from "../../public/images/partners/pact.png";
import howAreYou from "../../public/images/partners/howAreYou.png";
import israaid from "../../public/images/partners/israaid.png";
import academy from "../../public/images/partners/academy.png";

const HomeHero = () => {
    const { t } = useTranslation('common')

    const [popup, setPopup] = useState(false);
    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const targetRef = useRef()
    useEffect(()=> {
        popup ? disableBodyScroll(targetRef) : enableBodyScroll(targetRef);
        return () => {
            clearAllBodyScrollLocks();
        }
    }, [popup])

    const [layoutReady, setLayoutReady] = useState(false)
    useLayoutEffect(() => {
        const timeout = setTimeout(() => {
            setLayoutReady(true)
        }, 50)

        return () => clearTimeout(timeout)
    }, [])

    const partners = [un,pact,howAreYou,israaid,academy]

    return (<>
        <Parallax
            blur={{ min: -3, max: 3 }}
            bgImage='/images/kiev-city.jpg'
            bgImageAlt='kiev'
            strength={-100}
        >
            <div className={style.hero} >
                <div className='container'>
                    <div className={style.hero_title}>
                        <h1 ref={borderRef}>
                            {t("main-page.hero.h1")}
                        </h1>

                        <div className='buttons'>
                            <Link href="/help">
                                <a>
                                    <span>{t("main-page.hero.want-help")}</span>
                                </a>
                            </Link>


                            <a onClick={() => setPopup(true)}>
                                <span>{t("main-page.hero.need-help")}</span>
                            </a>
                        </div>

                        <a href='#scroll' className='arrow'>
                            <span/>
                        </a>
                    </div>

                    {layoutReady && <Contact popup={popup} setPopup={setPopup} targetRef={targetRef}/>}
                </div>
            </div>
        </Parallax>

        <div className={style.hero_partners}>
            <div className='container'>
                <h3>{t("main-page.hero.partners")}</h3>

                <div className={style.hero_partners_wrap}>
                    {partners?.map((img, idx) =>
                        <Link href='/about/partners'>
                            <div className={style.hero_partners_logo} key={idx}>
                                <Image
                                    src={img}
                                    layout='fill'
                                    alt='partner'
                                    objectFit='contain'
                                    loading='eager'
                                    placeholder="blur"
                                />
                            </div>
                        </Link>
                    )}
                </div>
            </div>
        </div>
    </>
    );
}


const Contact = ({popup, setPopup}) => {
    const { t } = useTranslation('common')

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');

    const [nameVisited, setNameVisited] = useState(false);
    const [emailVisited, setEmailVisited] = useState(false);
    const [messageVisited, setMessageVisited] = useState(false);

    const [formValid, setFormValid] = useState(false);
    const [nameError, setNameError] = useState(t("main-page.contact.name-empty"));
    const [emailError, setEmailError] = useState(t("main-page.contact.email-empty"));
    const [messageError, setMessageError] = useState(t("main-page.contact.message-empty"));

    const [onSubmit, setOnSubmit] = useState(false);

    const blurHandler = (e) => {
        switch (e.target.name) {
            case 'name':
                setNameVisited(true)
                break
            case 'email':
                setEmailVisited(true)
                break
            case 'message':
                setMessageVisited(true)
                break
        }
    }

    const emailHandler = (e) => {
        setEmail(e.target.value)
        const re =  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!re.test(String(e.target.value).toLowerCase())) {
            setEmailError(t("main-page.contact.incorrect-email"))
            if(!e.target.value) {
                setEmailError(t("main-page.contact.email-empty"))
            }
        } else {
            setEmailError('')
        }
    }

    const nameHandler = (e) => {
        setName(e.target.value)
        const re = /^[A-Za-zА-Яа-яёЁіІїЇґҐєЄ]+(?:[-'\s][A-Za-zА-Яа-яёЁіІїЇґҐєЄ]+)*$/;

        if (!e.target.value) {
            setNameError(t("main-page.contact.name-empty"))
        } else if (!re.test(String(e.target.value).toLowerCase())) {
            setNameError(t("main-page.contact.incorrect-name"))
        } else if (e.target.value.length < 3) {
            setNameError(t("main-page.contact.to-short"))
        } else if (e.target.value.length > 40) {
            setNameError(t("main-page.contact.to-long"))
            console.log(e.target.value.length)
        } else {
            setNameError('')
        }
    }

    const resetForm = () => {
        setName('')
        setEmail('')
        setMessage('')
    }

    const handleSubmit = e => {
        e.preventDefault();
        const data = {
            name,
            email,
            message,
        };
        fetch('/api/contact', {
            method: 'post',
            body: JSON.stringify(data),
        });
        resetForm();
        setOnSubmit(true);
    };

    useEffect(()=>{
        if (nameError || emailError || messageError) {
            setFormValid(false)
        } else {
            setFormValid(true)
        }
    },[nameError, emailError, messageError])


    return (
        <div className={`contact ` + (popup && 'popup')} onClick={() => setPopup(false)}>
            <div className="container">
                {!onSubmit ? (
                    <>
                        <h3>
                            {t("main-page.contact.title")}

                            <div className='contact_cross'/>
                        </h3>

                        <form
                            onSubmit={handleSubmit}
                            className='contact_form'
                            onClick={(e) => e.stopPropagation()}
                        >
                            <div className='contact_wrap'>
                                <div className='contact_inputs'>
                                    <div className='contact_item'>
                                        {(nameVisited && nameError) && <span className='contact_error'>
                                            {nameError}
                                        </span>}
                                        <input
                                            name='name'
                                            type="text"
                                            placeholder={t("main-page.contact.name")}
                                            value={name}
                                            onBlur={e => blurHandler(e)}
                                            onChange={e => nameHandler(e)}
                                        />
                                    </div>

                                    <div className='contact_item'>
                                        {(emailVisited && emailError) && <span className='contact_error'>
                                        {emailError}
                                    </span>}
                                        <input
                                            name='email'
                                            type="email"
                                            placeholder={t("main-page.contact.email")}
                                            value={email}
                                            onBlur={e => blurHandler(e)}
                                            onChange={e => emailHandler(e)}
                                        />
                                    </div>
                                </div>

                                <div className='contact_item'>
                                    {(messageVisited && messageError) && <span className='contact_error'>
                                    {messageError}
                                </span>}
                                    <textarea
                                        name='message'
                                        placeholder={t("main-page.contact.message")}
                                        // type="text"
                                        rows="4"
                                        onBlur={e => blurHandler(e)}
                                        value={message}
                                        onChange={e => {
                                            setMessage(e.target.value)
                                            if(!e.target.value) {
                                                setMessageError(t("main-page.contact.message-empty"))
                                            } else {
                                                setMessageError('')
                                            }
                                        }}
                                    />
                                </div>

                                <div className='contact_button'>
                                    <button
                                        disabled={!formValid || onSubmit}
                                        className='contact_button'
                                        type="submit"
                                    >
                                        {t("main-page.contact.send")}

                                        <a className='arrow'>
                                            <span/>
                                        </a>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </>)
                    : (<div className='contact_success'>
                            <h3>{t("main-page.contact.success")}</h3>

                            <svg width="101" height="101" viewBox="0 0 101 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="50.2645" cy="50.6763" r="49.2948" stroke="white"/>
                                <path d="M23.667 45.5116L49.6257 72.1763L81.2027 34.2616" stroke="white" strokeWidth="1.5"/>
                            </svg>

                            <div className='contact_cross success'/>
                       </div>
                    )
                }
            </div>
        </div>
    )
}

export { HomeHero };
