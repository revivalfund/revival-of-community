import Link from "next/link";
import Image from 'next/image';
import { useTranslation } from "next-i18next";

import style from './Home-about.module.scss';


// import banner from '../../public/images/team.jpeg';

const HomeAbout = () => {
    const { t } = useTranslation('common')

    return (
        <>
            <div className={style.about_banner}>
                <div className={style.about_banner_wrap}>
                    {/*<div className={style.about_banner_left}>*/}
                    {/*    <Image*/}
                    {/*        src={banner}*/}
                    {/*        layout='fill'*/}
                    {/*        alt='team'*/}
                    {/*        objectFit='cover'*/}
                    {/*    />*/}
                    {/*</div>*/}

                    <div className={style.about_banner_right}>
                        <div className={style.text}>
                            {/*<h2>{t("main-page.about.title")}</h2>*/}

                            <p><b>{t("main-page.about.text-begin")}</b></p>
                            <p>{t("main-page.about.text")}</p>
                        </div>

                        <Link href='/about/history'>
                            <a className={style.arrow}>
                                <span/>
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}

export { HomeAbout }
