import {useState} from "react";
import {useTranslation} from "next-i18next";
import {useEffect} from "react";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import OncePay from "../LiqPay/OncePay";
import SubsPay from "../LiqPay/SubsPay";


const Card = () => {
    const { t } = useTranslation('common')
    const [currency, setCurrency] = useState('');

    const [subs, setSubs] = useState(false);
    const [once, setOnce] = useState(false);

    const [sum, setSum] = useState('');
    const [otherSum, setOtherSum] = useState('');


    const [formValid, setFormValid] = useState(false);

    const [nameError, setNameError] = useState(t("main-page.contact.name-empty"));
    const [nameVisited, setNameVisited] = useState(false);
    const [name, setName] = useState('');

    const [email, setEmail] = useState('');
    const [emailVisited, setEmailVisited] = useState(false);
    const [emailError, setEmailError] = useState(t("main-page.contact.email-empty"));

    const [messageError, setMessageError] = useState(t("main-page.contact.message-empty"));
    const [messageVisited, setMessageVisited] = useState(false);

    const [onSubmit, setOnSubmit] = useState(false);

    const [checkbox, setCheckbox] = useState(false);


    const emailHandler = (e) => {
        setEmail(e.target.value)
        const re =  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!re.test(String(e.target.value).toLowerCase())) {
            setEmailError(t("main-page.contact.incorrect-email"))
            if(!e.target.value) {
                setEmailError(t("main-page.contact.email-empty"))
            }
        } else {
            setEmailError('')
        }
    }

    const blurHandler = (e) => {
        switch (e.target.name) {
            case 'name':
                setNameVisited(true)
                break
            case 'email':
                setEmailVisited(true)
                break
            case 'message':
                setMessageVisited(true)
                break
        }
    }

    const nameHandler = (e) => {
        setName(e.target.value)
        const re = /^[A-Za-zА-Яа-яёЁіІїЇґҐєЄ]+(?:[-'\s][A-Za-zА-Яа-яёЁіІїЇґҐєЄ]+)*$/;

        if (!e.target.value) {
            setNameError(t("main-page.contact.name-empty"))
        } else if (!re.test(String(e.target.value).toLowerCase())) {
            setNameError(t("main-page.contact.incorrect-name"))
        } else if (e.target.value.length < 3) {
            setNameError(t("main-page.contact.to-short"))
        } else if (e.target.value.length > 40) {
            setNameError(t("main-page.contact.to-long"))
            console.log(e.target.value.length)
        } else {
            setNameError('')
        }
    }

    const resetForm = () => {
        setName('')
        setEmail('')
        setMessage('')
    }

    const handleSubmit = e => {
        e.preventDefault();
        const data = {
            name,
            email,
            message,
        };
        // fetch('/api/contact', {
        //     method: 'post',
        //     body: JSON.stringify(data),
        // });
        resetForm();
        setOnSubmit(true);
    };

    useEffect(()=>{
        if (nameError || emailError || messageError) {
            setFormValid(false)
        } else {
            setFormValid(true)
        }
    },[nameError, emailError, messageError])


    const [startDate, setStartDate] = useState(new Date());

    return (
        <div className='help_content'>
            <h2>{t("help-page.h1")}</h2>

            <div className='help_card-form'>
                <div className='help_card-item'>
                    <div className='buttons buttons--payment-type'>
                        <a
                            onClick={() => {
                                setOnce(true)
                                setSubs(false)
                            }}
                            className={once ? 'active' : ''}
                        >
                            {t("help-page.once")}
                        </a>
                        <a
                            onClick={() => {
                                setOnce(false)
                                setSubs(true)
                            }}
                            className={subs ? 'active' : ''}
                        >
                            {t("help-page.subs")}
                        </a>
                    </div>
                </div>

                <div className='help_card-item'>
                    <h3>{t("help-page.currency")}</h3>
                    <div className='buttons'>
                        <a
                            onClick={() => {setCurrency('UAH')}}
                            className={currency==='UAH' ? 'active' : ''}
                        >
                            {t("help-page.uah")}
                        </a>
                        <a
                            onClick={() => {setCurrency('USD')}}
                            className={currency==='USD' ? 'active' : ''}
                        >
                            {t("help-page.usd")}
                        </a>
                        <a
                            onClick={() => {setCurrency('EUR')}}
                            className={currency==='EUR' ? 'active' : ''}
                        >
                            {t("help-page.eur")}
                        </a>
                    </div>
                </div>


                <div className='help_card-item'>
                    {once
                        ? <h3>{t("help-page.sum-type-once")}</h3>
                        : <h3>{t("help-page.sum-type-subs")}</h3>
                    }

                    <div className='buttons donate_sum'>
                        <a
                            onClick={() => {
                                setSum('50')
                                setOtherSum('')
                            }}
                            className={sum === '50' ? 'active' : ''}
                        >
                            50
                        </a>
                        <a
                            onClick={() => {
                                setSum('100')
                                setOtherSum('')
                            }}
                            className={sum === '100' ? 'active' : ''}
                        >
                            100
                        </a>
                        <a
                            onClick={() => {
                                setSum('200')
                                setOtherSum('')
                            }}
                            className={sum === '200' ? 'active' : ''}
                        >
                            200
                        </a>
                        <a
                            onClick={() => {
                                setSum('500')
                                setOtherSum('')
                            }}
                            className={sum === '500' ? 'active' : ''}
                        >
                            500
                        </a>
                        <a
                            onClick={() => {
                                setSum('1000')
                                setOtherSum('')
                            }}
                            className={sum === '1000' ? 'active' : ''}
                        >
                            1000
                        </a>

                        <a className={'otherSum' + (otherSum.length > 0 ? ' active' : '')}>
                            <input
                                type="number"
                                value={otherSum}
                                id="otherSum"
                                onChange={e => {
                                    setOtherSum(e.target.value)
                                    setSum('')
                                }}
                            />
                            <label
                                htmlFor="otherSum"
                                className={otherSum.length > 0 ? 'active' : ''}
                            >
                                <span>{t("help-page.other-sum")}</span>
                            </label>
                        </a>
                    </div>
                </div>

                {subs &&
                    <div className='help_card-item help_card-date'>
                        <h3>{t("help-page.payment-date")}</h3>

                        <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                    </div>}

                <div className='help_card-item'>
                    <h3>{t("help-page.personal-data")}</h3>

                    <div className='help_contact'>
                        <div className='contact_item'>
                            <input
                                name='name'
                                type="text"
                                placeholder={t("help-page.name")}
                                value={name}
                                onBlur={e => blurHandler(e)}
                                onChange={e => nameHandler(e)}
                            />
                            {(nameVisited && nameError) && <span className='contact_error'>
                                    {nameError}
                                </span>}
                        </div>
                        <div className='contact_item'>
                            <input
                                name='email'
                                type="email"
                                placeholder={t("main-page.contact.email")}
                                value={email}
                                onBlur={e => blurHandler(e)}
                                onChange={e => emailHandler(e)}
                            />
                            {(emailVisited && emailError) && <span className='contact_error'>
                                    {emailError}
                                </span>}
                        </div>
                    </div>
                </div>

                <div className='help_card-item help_checkbox'>
                    <label>
                        <span>{t("help-page.confirm")}<a href="/about/fund-documents#offer">{t("help-page.confirm-link")}</a></span>

                        <input type="checkbox" onClick={()=>setCheckbox(!checkbox)}/>
                        <span className="checkmark"/>
                    </label>
                </div>

                <div className='help_submit-wrap'>
                    {once &&
                        <OncePay
                            styleClass='help_submit'
                            title={t("help-page.donate")}
                            amount={sum || otherSum}
                            currency={currency.length > 0 && currency}
                            disabled={!checkbox || (!sum && !otherSum) || currency.length===0 || (email.length===0 || emailError || name.length === 0 || nameError)}
                            description={t("help-page.charity") + ', ' + name + ', ' + email}
                        />
                    }
                    {subs &&
                        <SubsPay
                            styleClass='help_submit'
                            title={t("help-page.donate")}
                            amount={sum || otherSum}
                            currency={currency}
                            subscribeStart={Date.parse(startDate)}
                            disabled={!checkbox || (!sum && !otherSum) || (email.length===0 || emailError || name.length === 0 || nameError)}
                        />
                    }
                </div>
            </div>
        </div>
    );
}

export {Card};
