import {useState} from "react";
import {useTranslation} from "next-i18next";
import {useRouter} from "next/router";


const Bank = () => {
    const { t } = useTranslation('common')
    const router = useRouter();

    const [UAH, setUAH] = useState(true);
    const [EUR, setEUR] = useState(false);
    const [USD, setUSD] = useState(false);
    const [copy, setCopy] = useState('');

    return (
        <div className='help_content'>
            <h2>{t("help-page.make-bank")}</h2>

            <div className='buttons'>
                <a
                    onClick={() => {
                        setUAH(true)
                        setUSD(false)
                        setEUR(false)
                    }}
                    className={UAH ? 'active' : ''}
                >
                    {t("help-page.uah")}
                </a>
                <a
                    onClick={() => {
                        setUAH(false)
                        setUSD(true)
                        setEUR(false)
                    }}
                    className={USD ? 'active' : ''}
                >
                    {t("help-page.usd")}
                </a>
                <a
                    onClick={() => {
                        setUAH(false)
                        setUSD(false)
                        setEUR(true)
                    }}
                    className={EUR ? 'active' : ''}
                >
                    {t("help-page.eur")}
                </a>
            </div>

            {UAH &&
                <div className='help_payment-data'>
                    {router.locale === 'ua' ? <>
                            <h4>БЛАГОДІЙНА ОРГАНІЗАЦІЯ «ВІДРОДЖЕННЯ ГРОМАД»</h4>

                            <PaymentItem text={'Ідентифікаційний код: 44770808'} copy={copy} setCopy={setCopy} copyName={'inn'} value={'44770808'}/>
                            <PaymentItem text={'IBAN: UA903052990000026006016231912'} copy={copy} setCopy={setCopy} copyName={'iban'} value={'UA903052990000026006016231912'}/>
                            <PaymentItem text={'АТ КБ «ПРИВАТБАНК»'} copy={copy} setCopy={setCopy} copyName={'bank'} value={'АТ КБ «ПРИВАТБАНК»'}/>
                            <PaymentItem text={'Призначення платежу – Благодійна допомога'} copy={copy} setCopy={setCopy} copyName={'purpose'} value={'Благодійна допомога'}/>
                        </>
                        : <>
                            <h4>CHARITable ORGANIZATION "COMMUNITIES REVIVAL"</h4>

                            <PaymentItem text={'Identification code: 44770808'} copy={copy} setCopy={setCopy} copyName={'inn'} value={'44770808'}/>
                            <PaymentItem text={'IBAN: UA903052990000026006016231912'} copy={copy} setCopy={setCopy} copyName={'iban'} value={'UA903052990000026006016231912'}/>
                            <PaymentItem text={'JSC CB "PRIVATBANK" in Kyiv'} copy={copy} setCopy={setCopy} copyName={'bank'} value={'JSC CB "PRIVATBANK"'}/>
                            <PaymentItem text={'Purpose of payment: Charitable donation'} copy={copy} setCopy={setCopy} copyName={'purpose'} value={'Charitable donation'}/>
                        </>}
                </div>}

            {USD &&
                <div className='help_payment-data'>
                    <h4>Company details: Charitable Organization “Communities Revival”</h4>

                    <PaymentItem text={'IBAN Code: UA723052990000026005026235686'} copy={copy} setCopy={setCopy} copyName={'iban-usd'} value={'UA723052990000026005026235686'}/>
                    <PaymentItem text={'Name of the bank: JSC CB "PRIVATBANK", 1D HRUSHEVSKOHO STR., KYIV, 01001, UKRAINE'} copy={copy} setCopy={setCopy} copyName={'bank-usd'} value={'JSC CB "PRIVATBANK"'}/>
                    <PaymentItem text={'Bank SWIFT Code: PBANUA2X'} copy={copy} setCopy={setCopy} copyName={'swift-usd'} value={'PBANUA2X'}/>
                    <PaymentItem text={'Correspondent bank: JP Morgan Chase Bank, New York ,USA'} copy={copy} setCopy={setCopy} copyName={'correspondent-name-usd'} value={'JP Morgan Chase Bank'}/>
                    <PaymentItem text={'Account in the correspondent bank: 001-1- 000080'} copy={copy} setCopy={setCopy} copyName={'account-usd'} value={'001-1- 000080'}/>
                    <PaymentItem text={'SWIFT Code of the correspondent bank: CHASUS33'} copy={copy} setCopy={setCopy} copyName={'correspondent-swift-usd'} value={'CHASUS33'}/>
                    <PaymentItem text={'Purpose of payment: Charitable donation'} copy={copy} setCopy={setCopy} copyName={'purpose-usd'} value={'Charitable donation'}/>
                </div>}

            {EUR &&
                <div className='help_payment-data'>
                    <h4>Company details: Charitable Organization “Communities Revival”</h4>

                    <PaymentItem text={'IBAN Code: UA913052990000026009016224517'} copy={copy} setCopy={setCopy} copyName={'iban-eur'} value={'UA913052990000026009016224517'}/>
                    <PaymentItem text={'Name of the bank: JSC CB "PRIVATBANK", 1D HRUSHEVSKOHO STR., KYIV, 01001, UKRAINE'} copy={copy} setCopy={setCopy} copyName={'bank-eur'} value={'JSC CB "PRIVATBANK"'}/>
                    <PaymentItem text={'Bank SWIFT Code: PBANUA2X'} copy={copy} setCopy={setCopy} copyName={'swift-eur'} value={'PBANUA2X'}/>
                    <PaymentItem text={'Correspondent bank Commerzbank AG, Frankfurt am Main, Germany'} copy={copy} setCopy={setCopy} copyName={'correspondent-name-eur'} value={'Commerzbank AG'}/>
                    <PaymentItem text={'Account in the correspondent bank: 400886700401'} copy={copy} setCopy={setCopy} copyName={'account-eur'} value={'400886700401'}/>
                    <PaymentItem text={'SWIFT Code of the correspondent bank: COBADEFF'} copy={copy} setCopy={setCopy} copyName={'correspondent-swift-eur'} value={'COBADEFF'}/>
                    <PaymentItem text={'Purpose of payment: Charitable donation'} copy={copy} setCopy={setCopy} copyName={'purpose-eur'} value={'Charitable donation'}/>
                </div>}
        </div>
    );
}

export {Bank};


const PaymentItem = ({text, copy, setCopy, copyName, value}) => {

    return (
        <p onClick={() => {
            navigator.clipboard.writeText(value)
                .then(el => setCopy(copyName))
            }}
        >
            {text}
            <div className={'help_copy ' + (copy === copyName && 'active')}>
                <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="copy" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path d="M433.941 65.941l-51.882-51.882A48 48 0 0 0 348.118 0H176c-26.51 0-48 21.49-48 48v48H48c-26.51 0-48 21.49-48 48v320c0 26.51 21.49 48 48 48h224c26.51 0 48-21.49 48-48v-48h80c26.51 0 48-21.49 48-48V99.882a48 48 0 0 0-14.059-33.941zM266 464H54a6 6 0 0 1-6-6V150a6 6 0 0 1 6-6h74v224c0 26.51 21.49 48 48 48h96v42a6 6 0 0 1-6 6zm128-96H182a6 6 0 0 1-6-6V54a6 6 0 0 1 6-6h106v88c0 13.255 10.745 24 24 24h88v202a6 6 0 0 1-6 6zm6-256h-64V48h9.632c1.591 0 3.117.632 4.243 1.757l48.368 48.368a6 6 0 0 1 1.757 4.243V112z"/>
                </svg>
            </div>
        </p>
    )
}