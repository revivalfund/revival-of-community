import { utf8_to_b64 } from './utf8-b64'
import crypto from 'crypto'
import {useRouter} from "next/router";


const SubsPay = ({
    // publicKey='sandbox_i1335177235',
    publicKey='i22402410033',
    // privateKey='sandbox_acFljTI2lxr5YOHo7NT6QnAgaTyO1ABSAWRD4yUe',
    privateKey='szFjWTNK8ZNL1HfxrmlPWzqxj02cyY7nHWgD6SmP',
    amount,
    subscribePeriodicity = 'month',
    subscribeStart = Date.now(),
    currency='UAH',
    description = 'Charity',
    orderId = Math.floor(1 + Math.random() * 900000000),
    title = 'Subscribe',
    disabled,
    styleClass,
    ...props
}) => {
    const router = useRouter();
    const jsonString = {
        public_key: publicKey,
        version: '3',
        action: 'subscribe',
        subscribe_date_start: subscribeStart,
        subscribe_periodicity: subscribePeriodicity,
        amount: amount,
        currency: currency,
        description: description,
        order_id: orderId,
        language: router.locale,
        ...props
    }

    const data = utf8_to_b64(JSON.stringify(jsonString))
    const signString = privateKey + data + privateKey

    const sha1 = crypto.createHash('sha1')
    sha1.update(signString)
    const signature = sha1.digest('base64')

    return (
        <form
            method='POST' action='https://www.liqpay.ua/api/3/checkout'
            acceptCharset='utf-8'
            target='_blank'
            rel='noreferrer nofollow'
            className={styleClass ? styleClass : ''}
        >
            <input type='hidden' name='data' value={data}/>
            <input type='hidden' name='signature' value={signature} />

            <button disabled={disabled}>
                <span>{title}</span>
            </button>
        </form>
    )
}

export default SubsPay