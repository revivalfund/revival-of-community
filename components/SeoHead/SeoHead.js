import Head from "next/head";

import logo from '../../public/images/logo_ua.png';

const SeoHead = ({ seo }) => {

    return (
        <Head>
            <title>{seo?.seo_title}</title>
            <meta name="description" content={seo?.seo_description}/>
            <meta property="og:title" content={seo?.og_title} />
            <meta property="og_image" content={logo} />
            <meta property="og_description" content={seo?.og_description} />
        </Head>
    )
}

export { SeoHead };