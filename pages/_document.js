import Document, { Html, Head, Main, NextScript } from 'next/document'
import React from "react";
import i18nextConfig from '../next-i18next.config'

export default class MyDocument extends Document {
    render() {
        const currentLocale = this.props.__NEXT_DATA__.locale || i18nextConfig.i18n.defaultLocale
        const GA_TRACKING_ID = 'G-V3P9LHVR0Y';

        return (
            <Html lang={currentLocale}>
                <Head>
                    {/*Google tag (gtag.js)*/}
                    <script
                        async
                        src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
                    />
                    <script
                        dangerouslySetInnerHTML={{
                            __html: `
                                window.dataLayer = window.dataLayer || [];
                                function gtag(){dataLayer.push(arguments);}
                                gtag('js', new Date());
                                gtag('config', '${GA_TRACKING_ID}', {
                                  page_path: window.location.pathname,
                                });
                              `,
                        }}
                    />

                    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/>
                    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
                    <link rel="shortcut icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
                    <link rel="manifest" href="/site.webmanifest" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}
