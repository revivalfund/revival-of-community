import { Layout } from "../../components/Layout";
import { useState } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useInView } from "react-intersection-observer";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { useTranslation } from "next-i18next";
import {Bank} from "../../components/Help/Bank";
import {Card} from "../../components/Help/Card";
import OncePay from "../../components/LiqPay/OncePay";


const Help = () => {
    const { t } = useTranslation('common')

    const [cardPayment, setCardPayment] = useState(true);

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("seo.help.title"),
        seo_description: t("seo.help.description"),
        og_title: t("seo.help.title"),
        og_description: t("seo.help.description")
    }

    return <Layout seo={seo}>
        <div className='help section'>
            <div className="container">
                <h1 ref={borderRef}>{t("help-page.h1")}</h1>

                <div className='help_wrap'>
                    <h3>{t("help-page.payment-type")}</h3>

                    <div className='buttons'>
                        <a
                            onClick={() => setCardPayment(true)}
                            className={cardPayment ? 'active' : ''}
                        >
                            {t("help-page.card")}
                        </a>
                        <a
                            onClick={() => setCardPayment(false)}
                            className={!cardPayment ? 'active' : ''}
                        >
                            {t("help-page.bank")}
                        </a>
                    </div>

                    {cardPayment ? <Card /> : <Bank />}
                </div>
            </div>
        </div>
    </Layout>

}

export default Help;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})