import { Layout } from "../components/Layout";
import Link from "next/link";
import Image from 'next/image';

import error from '../public/images/error.jpg';
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import {useTranslation} from "next-i18next";

const Error = () => {
    const { t } = useTranslation('common')

    return <Layout>
        <div className='error section'>
            <div className='error-wrap'>
                <Image
                    src={error}
                    layout='fill'
                    alt='error'
                    objectFit='cover'
                />

                <h1>{t("404.title")}</h1>
                <div className='buttons'>
                    <Link href="/">
                        <a>{t("404.back")}</a>
                    </Link>
                </div>
            </div>
        </div>
    </Layout>

}

export default Error;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})