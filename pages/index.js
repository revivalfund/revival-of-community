import { Layout } from '../components/Layout';
import { HomeHero } from '../components/Home-Hero';
import { HomeAbout } from '../components/Home-About';
import { HomeActivities } from "../components/Home-Activities";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import {useTranslation} from "next-i18next";
import { HomeTeam } from "../components/Home-Team";


export default function Home() {
    const { t } = useTranslation('common')

    const seo = {
        seo_title: t("seo.main.title"),
        seo_description: t("seo.main.description"),
        og_title: t("seo.main.title"),
        og_description: t("seo.main.description")
    }

    return (
        <Layout seo={seo}>
            <HomeHero/>
            <a name='scroll'/>

            <HomeAbout/>
            <HomeActivities/>
            {/*<HomeTeam/>*/}
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})