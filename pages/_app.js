import '../styles/globals.scss'

import { Provider } from 'react-redux'
import { initializeStore, useStore, wrapper } from "../store";
import App from "next/app";
import NextNProgress from "nextjs-progressbar";
import { appWithTranslation } from 'next-i18next';

function MyApp({Component, pageProps}) {
    const store = useStore(pageProps.initialReduxState)

    return (
        <Provider store={store}>
            <NextNProgress
                color="#eeb62c"
                startPosition={0.3}
                stopDelayMs={200}
                height={4}
                showOnShallow={true}
            />

            <Component {...pageProps} />
        </Provider>
    )
}

MyApp.getInitialProps = async (appContext) => {
    const appProps = await App.getInitialProps(appContext);
    const reduxStore = initializeStore({});

    appProps.pageProps = {
        ...appProps.pageProps,
        initialReduxState: reduxStore.getState(),
    };
    return appProps;
}

export default appWithTranslation(wrapper.withRedux(MyApp));

