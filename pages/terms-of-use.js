import { Layout } from "../components/Layout";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useInView } from "react-intersection-observer";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { useTranslation } from "next-i18next";

const Terms = () => {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    return <Layout>
        <div className='terms section'>
            <div className="container">
                <div className='terms-wrap'>
                    <h1 ref={borderRef}>{t("terms.title1")}</h1>

                    <p>{t("terms.p1")}</p>

                    <h2>{t("terms.title2")}</h2>
                    <p>{t("terms.p2")}</p>

                    <h2>{t("terms.title3")}</h2>
                    <p>{t("terms.p3")}<a href="mailto:revivalfund2022@gmail.com">revivalfund2022@gmail.com</a>.
                    </p>

                    <h2>{t("terms.title4")}</h2>
                    <p>{t("terms.p4")}</p>

                    <h2>{t("terms.title5")}</h2>
                    <p>{t("terms.p5")}</p>

                    <h2>{t("terms.title6")}</h2>
                    <p>{t("terms.p6")}</p>

                    <h2>{t("terms.title7")}</h2>
                    <p>{t("terms.p7")}</p>

                    <h2>{t("terms.title8")}</h2>
                    <p>{t("terms.p8")}</p>

                    <h2>{t("terms.title9")}</h2>
                    <p>{t("terms.p9")}</p>
                </div>
            </div>
        </div>
    </Layout>

}

export default Terms;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})