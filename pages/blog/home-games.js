import { Layout } from "../../components/Layout";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {useEffect} from "react";
import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import Image from "next/image";
import BlogSlider from "../../components/Blog/BlogSlider";

import title from "../../public/images/blog/home-games/title.jpg";
import img1 from "../../public/images/blog/home-games/img1.webp";
import img2 from "../../public/images/blog/home-games/img2.webp";
import img3 from "../../public/images/blog/home-games/img3.jpg";
import img4 from "../../public/images/blog/home-games/img4.jpg";
import img5 from "../../public/images/blog/home-games/img5.jpg";
import img6 from "../../public/images/blog/home-games/img6.jpg";
import img7 from "../../public/images/blog/home-games/img7.jpg";
import img8 from "../../public/images/blog/home-games/img8.jpg";
import img9 from "../../public/images/blog/home-games/img9.jpg";
import img10 from "../../public/images/blog/home-games/img10.jpg";


const HomeGames = () => {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("blog.home-games.meta-title"),
        seo_description: t("blog.home-games.meta-description"),
        og_title: t("blog.home-games.meta-title"),
        og_description: t("blog.home-games.meta-description")
    }

    const reportArr1 = [
        {
            dir: '',
            name: t("blog.home-games.table.donat1.col2.row1"),
            sum: t("blog.home-games.table.donat1.col3.row1")
        },
        {
            dir: t("blog.home-games.table.donat1.col1.row2"),
            name: '',
            sum: ''
        },
        {
            dir: '',
            name: t("blog.home-games.table.donat1.col2.row3"),
            sum: t("blog.home-games.table.donat1.col3.row3")
        },
        {
            dir: '',
            name: t("blog.home-games.table.donat1.col2.row4"),
            sum: t("blog.home-games.table.donat1.col3.row4")
        },
        {
            dir: '',
            name: t("blog.home-games.table.donat1.col2.row5"),
            sum: t("blog.home-games.table.donat1.col3.row5")
        },
        {
            dir: '',
            name: t("blog.home-games.table.donat1.col2.row6"),
            sum: t("blog.home-games.table.donat1.col3.row6")
        },
        {
            dir: '',
            name: t("blog.home-games.table.donat1.col2.row7"),
            sum: t("blog.home-games.table.donat1.col3.row7")
        },
        {
            dir: '',
            name: t("blog.home-games.table.donat1.col2.row8"),
            sum: t("blog.home-games.table.donat1.col3.row8")
        },
        {
            dir: '',
            name: '',
            sum: t("blog.home-games.table.donat1.col3.row9")
        },
    ]

    const reportArr2 = [
        {
            date: t("blog.home-games.table.donat2.col1.row1"),
            context: t("blog.home-games.table.donat2.col2.row1"),
            sum: t("blog.home-games.table.donat2.col3.row1")
        },
        {
            date: t("blog.home-games.table.donat2.col1.row2"),
            context: t("blog.home-games.table.donat2.col2.row2"),
            sum: t("blog.home-games.table.donat2.col3.row2")
        },
        {
            date: t("blog.home-games.table.donat2.col1.row3"),
            context: t("blog.home-games.table.donat2.col2.row3"),
            sum: t("blog.home-games.table.donat2.col3.row3")
        },
        {
            date: t("blog.home-games.table.donat2.col1.row4"),
            context: t("blog.home-games.table.donat2.col2.row4"),
            sum: t("blog.home-games.table.donat2.col3.row4")
        },
        {
            date: '',
            context: t("blog.home-games.table.donat2.col2.row5"),
            sum: t("blog.home-games.table.donat2.col3.row5")
        },
        {
            date: '',
            context: t("blog.home-games.table.donat2.col2.row6"),
            sum: t("blog.home-games.table.donat2.col3.row6")
        },
        {
            date: '',
            context: t("blog.home-games.table.donat2.col2.row7"),
            sum: t("blog.home-games.table.donat2.col3.row7")
        },
        {
            date: '',
            context: t("blog.home-games.table.donat2.col2.row8"),
            sum: t("blog.home-games.table.donat2.col3.row8")
        },
        {
            date: '',
            context: t("blog.home-games.table.donat2.col2.row9"),
            sum: t("blog.home-games.table.donat2.col3.row9")
        },
        {
            date: '',
            context: t("blog.home-games.table.donat2.col2.row10"),
            sum: t("blog.home-games.table.donat2.col3.row10")
        },
        {
            date: t("blog.home-games.table.donat2.col1.row11"),
            context: t("blog.home-games.table.donat2.col2.row11"),
            sum: t("blog.home-games.table.donat2.col3.row11")
        },
        {
            date: '',
            context: t("blog.home-games.table.donat2.col2.row12"),
            sum: t("blog.home-games.table.donat2.col3.row12")
        },
        {
            date: '',
            context: t("blog.home-games.table.donat2.col2.row13"),
            sum: t("blog.home-games.table.donat2.col3.row13")
        },
    ]

    const reportArr3 = [
        {
            dir: '',
            name: t("blog.home-games.table.donat3.col2.row1"),
            sum: t("blog.home-games.table.donat3.col3.row1")
        },
        {
            dir: t("blog.home-games.table.donat3.col1.row2"),
            name: '',
            sum: ''
        },
        {
            dir: '',
            name: t("blog.home-games.table.donat3.col2.row3"),
            sum: t("blog.home-games.table.donat3.col3.row3")
        },
        {
            dir: '',
            name: t("blog.home-games.table.donat3.col2.row4"),
            sum: t("blog.home-games.table.donat3.col3.row4")
        },
        {
            dir: '',
            name: t("blog.home-games.table.donat3.col2.row5"),
            sum: t("blog.home-games.table.donat3.col3.row5")
        },
        {
            dir: '',
            name: '',
            sum: t("blog.home-games.table.donat3.col3.row6")
        },
    ]

    const imgArr = [{img: img1}, {img: img2}, {img: img3}, {img: img4}, {img: img5}, {img: img6}, {img: img7}, {img: img8}, {img: img9}, {img: img10}]

    return <Layout seo={seo}>
        <div className='blog section'>
            <div className="container">
                <h1 ref={borderRef}>{t("blog.home-games.title")}</h1>
                <span className='blog__date'>{t("blog.home-games.date")}</span>

                <div className='blog__image'>
                    <Image
                        src={title}
                        layout='fill'
                        alt='blogHero'
                        objectFit='cover'
                        loading='eager'
                        placeholder="blur"
                    />
                </div>

                <div className="blog__text">
                    <div
                        className="blog__text"
                        dangerouslySetInnerHTML={{
                            __html: t("blog.home-games.text")
                        }}
                    />

                    <h2 className='bold'>{t("blog.home-games.table.donat1.name")}</h2>
                    <div className='overflow-table'>
                        <table>
                            <thead>
                            <tr>
                                <th scope="col">№</th>
                                <th scope="col">{t("blog.home-games.table.donat1.col1.header")}</th>
                                <th scope="col">{t("blog.home-games.table.donat1.col2.header")}</th>
                                <th scope="col">{t("blog.home-games.table.donat1.col3.header")}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {reportArr1.map((rep, i) =>
                                <tr key={i}>
                                    <td scope="row">{i + 1}</td>
                                    <td>{rep.dir}</td>
                                    <td>{rep.name}</td>
                                    <td className='block-sum'>{rep.sum}</td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </div>

                    <h2 className='bold'>{t("blog.home-games.table.donat2.name")}</h2>
                    <div className='overflow-table'>
                        <table>
                            <thead>
                            <tr>
                                <th scope="col">{t("blog.home-games.table.donat2.col1.header")}</th>
                                <th scope="col">{t("blog.home-games.table.donat2.col2.header")}</th>
                                <th scope="col">{t("blog.home-games.table.donat2.col3.header")}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {reportArr2.map((rep, i) =>
                                <tr key={i}>
                                    <td scope="row">{rep.date}</td>
                                    <td>{rep.context}</td>
                                    <td className='block-sum'>{rep.sum}</td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </div>

                    <h2 className='bold'>{t("blog.home-games.table.donat3.name")}</h2>
                    <div className='overflow-table'>
                        <table>
                            <thead>
                            <tr>
                                <th scope="col">№</th>
                                <th scope="col">{t("blog.home-games.table.donat3.col1.header")}</th>
                                <th scope="col">{t("blog.home-games.table.donat3.col2.header")}</th>
                                <th scope="col">{t("blog.home-games.table.donat3.col3.header")}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {reportArr3.map((rep, i) =>
                                <tr key={i}>
                                    <td scope="row">{i + 1}</td>
                                    <td>{rep.dir}</td>
                                    <td>{rep.name}</td>
                                    <td className='block-sum'>{rep.sum}</td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </div>
                </div>

                <BlogSlider images={imgArr}/>
            </div>
        </div>
    </Layout>
}

export default HomeGames;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})