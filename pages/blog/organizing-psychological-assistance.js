import {useTranslation} from "next-i18next";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect, useState} from "react";
import Image from "next/image";
import {Swiper, SwiperSlide} from "swiper/react";
import {Autoplay} from "swiper";
import {Layout} from "../../components/Layout";

import img1 from "../../public/images/blog/psychological-assistance/img1.png";
import img2 from "../../public/images/blog/psychological-assistance/img2.png";
import img3 from "../../public/images/blog/psychological-assistance/img3.png";
import img4 from "../../public/images/blog/psychological-assistance/img4.png";
import img5 from "../../public/images/blog/psychological-assistance/img5.png";
import img6 from "../../public/images/blog/psychological-assistance/img6.png";
import img7 from "../../public/images/blog/psychological-assistance/img7.png";

import "swiper/css";


const OrganizingPsychologicalAssistance = () => {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("blog.psychological-assistance.meta-title"),
        seo_description: t("blog.psychological-assistance.meta-description"),
        og_title: t("blog.psychological-assistance.meta-title"),
        og_description: t("blog.psychological-assistance.meta-description")
    }

    const [navSwiper, setNavSwiper] = useState(null);

    const imgArr = [{img: img1}, {img: img2}, {img: img3}, {img: img4}, {img: img5}, {img: img6}, {img: img7}]


    return <Layout seo={seo}>
        <div className='blog section back-home'>
            <div className="container">
                <h1 ref={borderRef}>{t("blog.psychological-assistance.title")}</h1>
                <span className='blog__date'>{t("blog.psychological-assistance.date")}</span>

                <div className='blog__image'>
                    <Image
                        src={img1}
                        layout='fill'
                        alt='blogHero'
                        objectFit='cover'
                        loading='eager'
                        placeholder="blur"
                    />
                </div>

                <div
                    className="blog__text project-article"
                    dangerouslySetInnerHTML={{
                        __html: t("blog.psychological-assistance.text")
                    }}
                />

                <div className='blog__slider'>
                    <Swiper
                        onSwiper={setNavSwiper}
                        slidesPerView={1}
                        breakpoints={{
                            640: {
                                slidesPerView: 2,
                            },
                            1024: {
                                slidesPerView: 3,
                            },
                        }}
                        spaceBetween={20}
                        autoplay={{
                            delay: 2500,
                            disableOnInteraction: false,
                        }}
                        modules={[Autoplay]}
                    >
                        {imgArr.map((el, i) =>
                            <SwiperSlide className='blog__slide relative'>
                                <Image
                                    src={el.img}
                                    layout='fill'
                                    alt='reports'
                                    objectFit='cover'
                                    loading='eager'
                                    placeholder="blur"
                                />
                            </SwiperSlide>
                        )}
                    </Swiper>
                </div>
            </div>
        </div>
    </Layout>
}

export default OrganizingPsychologicalAssistance;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})