import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {Layout} from "../../components/Layout";
import Image from "next/image";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";


const ReportingFor2022 = () => {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("blog.reporting-for-2022.meta-title"),
        seo_description: t("blog.reporting-for-2022.meta-description"),
        og_title: t("blog.reporting-for-2022.meta-title"),
        og_description: t("blog.reporting-for-2022.meta-description")
    }


    return <Layout seo={seo}>
        <div className='blog section'>
            <div className="container">
                <h1 ref={borderRef}>{t("blog.reporting-for-2022.title")}</h1>
                <span className='blog__date'>{t("blog.reporting-for-2022.date")}</span>

                <div
                    className="blog__text mr-3"
                    dangerouslySetInnerHTML={{
                        __html: t("blog.reporting-for-2022.text")
                    }}
                />

                <div className='blog__documents'>
                    <iframe src="https://drive.google.com/file/d/17Df5IOXdmpyWGpefMYvdi6W1MSYuiVkd/preview" allow="autoplay"/>
                    <iframe src="https://drive.google.com/file/d/1WBlKvHoIdn4cm9-imll4LP--EZDh7AA5/preview" allow="autoplay"/>
                </div>
            </div>
        </div>
    </Layout>
}

export default ReportingFor2022;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})