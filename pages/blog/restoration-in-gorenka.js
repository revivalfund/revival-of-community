import { Layout } from "../../components/Layout";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {useEffect} from "react";
import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";


const RestorationInGorenka = () => {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("blog.restoration-in-gorenka.title"),
        seo_description: t("blog.restoration-in-gorenka.meta"),
        og_title: t("blog.restoration-in-gorenka.title"),
        og_description: t("blog.restoration-in-gorenka.meta")
    }

    return <Layout seo={seo}>
        <div className='blog section'>
            <div className="container">
                <h1 ref={borderRef}>{t("blog.restoration-in-gorenka.title")}</h1>
                <span className='blog__date'>{t("blog.restoration-in-gorenka.date")}</span>
                <br/>
                <div
                    className="blog__text"
                    dangerouslySetInnerHTML={{
                        __html: t("blog.restoration-in-gorenka.text")
                    }}
                />
            </div>
        </div>
    </Layout>
}

export default RestorationInGorenka;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})