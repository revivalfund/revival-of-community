import { Layout } from "../../components/Layout";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {useEffect, useState} from "react";
import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useRouter} from "next/router";
import Image from "next/image";

import blog1 from "../../public/images/blog/how-to-help-Ukrain/how-to-help-Ukrain.jpg";


const HowToHelpUkraine = () => {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("blog.how-to-help-Ukraine.title"),
        seo_description: t("blog.how-to-help-Ukraine.meta"),
        og_title: t("blog.how-to-help-Ukraine.title"),
        og_description: t("blog.how-to-help-Ukraine.meta")
    }

    return <Layout seo={seo}>
        <div className='blog section'>
            <div className="container">
                <h1 ref={borderRef}>{t("blog.how-to-help-Ukraine.title")}</h1>
                <span className='blog__date'>{t("blog.how-to-help-Ukraine.date")}</span>

                <div className='blog__image'>
                    <Image
                        src={blog1}
                        layout='fill'
                        alt='blogHero'
                        objectFit='cover'
                        loading='eager'
                        placeholder="blur"
                    />
                </div>

                <div
                    className="blog__text"
                    dangerouslySetInnerHTML={{
                        __html: t("blog.how-to-help-Ukraine.text")
                    }}
                />
            </div>
        </div>
    </Layout>
}

export default HowToHelpUkraine;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})