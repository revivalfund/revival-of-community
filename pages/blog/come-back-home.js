import { Layout } from "../../components/Layout";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {useEffect} from "react";
import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import Image from "next/image";
import {Swiper, SwiperSlide} from "swiper/react";
import {Autoplay} from "swiper";
import {useState} from "react";

import title from "../../public/images/blog/back-home/title.jpg";
import img1 from "../../public/images/blog/back-home/img1.jpg";
import img2 from "../../public/images/blog/back-home/img2.jpg";
import img3 from "../../public/images/blog/back-home/img3.jpg";
import img4 from "../../public/images/blog/back-home/img4.jpg";
import img5 from "../../public/images/blog/back-home/img5.jpg";
import img6 from "../../public/images/blog/back-home/img6.jpg";
import img7 from "../../public/images/blog/back-home/img7.jpg";
import img8 from "../../public/images/blog/back-home/img8.jpg";
import img9 from "../../public/images/blog/back-home/img9.jpg";
import img10 from "../../public/images/blog/back-home/img10.jpg";
import img11 from "../../public/images/blog/back-home/img11.jpg";
import img12 from "../../public/images/blog/back-home/img12.jpg";
import img13 from "../../public/images/blog/back-home/img13.jpg";
import img14 from "../../public/images/blog/back-home/img14.jpg";
import img15 from "../../public/images/blog/back-home/img15.jpg";
import img16 from "../../public/images/blog/back-home/img16.jpg";
import donor from "../../public/images/blog/back-home/donor.png";

import "swiper/css";


const BackHome = () => {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("blog.home-games.meta-title"),
        seo_description: t("blog.home-games.meta-description"),
        og_title: t("blog.home-games.meta-title"),
        og_description: t("blog.home-games.meta-description")
    }

    const reportArr1 = [
        {
            expectAction: t("blog.back-home.table.header.expectAction"),
            expectResult: t("blog.back-home.table.header.expectResult"),
            action: t("blog.back-home.table.header.action"),
            date: t("blog.back-home.table.header.date"),
            result: t("blog.back-home.table.header.result"),
            product: t("blog.back-home.table.header.product"),
        },
        {
            expectAction: t("blog.back-home.table.rep1.expectAction"),
            expectResult: t("blog.back-home.table.rep1.expectResult"),
            action: t("blog.back-home.table.rep1rep1.action"),
            date: t("blog.back-home.table.rep1.date"),
            result: t("blog.back-home.table.rep1.result"),
            product: t("blog.back-home.table.rep1.product"),
        },
        {
            expectAction: t("blog.back-home.table.rep2.expectAction"),
            expectResult: t("blog.back-home.table.rep2.expectResult"),
            action: t("blog.back-home.table.rep2.action"),
            date: t("blog.back-home.table.rep2.date"),
            result: t("blog.back-home.table.rep2.result"),
            product: t("blog.back-home.table.rep2.product"),
        },
    ]
    const [navSwiper, setNavSwiper] = useState(null);

    const imgArr = [{img: img1}, {img: img2}, {img: img3}, {img: img4}, {img: img5}, {img: img6}, {img: img7}, {img: img8},
        {img: img9}, {img: img10}, {img: img11}, {img: img12}, {img: img13}, {img: img14}, {img: img15}, {img: img16}]


    return <Layout seo={seo}>
        <div className='blog section back-home'>
            <div className="container">
                <h1 ref={borderRef}>{t("blog.back-home.title")}</h1>
                <span className='blog__date'>{t("blog.back-home.date")}</span>

                <div className='blog__image'>
                    <Image
                        src={title}
                        layout='fill'
                        alt='blogHero'
                        objectFit='cover'
                        loading='eager'
                        placeholder="blur"
                    />
                </div>

                <div className="blog__text">
                    <div
                        className="blog__text"
                        dangerouslySetInnerHTML={{
                            __html: t("blog.back-home.text")
                        }}
                    />


                    <div className='back-home__wrap'>
                        <div className='back-home__table'>
                            <div className='cell'>{t("blog.back-home.table.header.titleLeft")}</div>
                            <div className='cell br0'>{t("blog.back-home.table.header.titleRight")}</div>

                            {reportArr1.map((rep, i) =>
                                <>
                                    <div className='back-home__left'>
                                        <div className={'cell' + (i + 1 === reportArr1.length ? ' bb0' : '')}>
                                            {i === 0 ? '№' : i}
                                        </div>
                                        <div className={'cell' + (i + 1 === reportArr1.length ? ' bb0' : '')}>
                                            {rep.expectAction}
                                        </div>
                                        <div className={'cell' + (i + 1 === reportArr1.length ? ' bb0' : '')}>
                                            {rep.expectResult}
                                        </div>
                                    </div>

                                    <div className='back-home__right'>
                                        <div className={'cell' + (i + 1 === reportArr1.length ? ' bb0' : '')}>
                                            {rep.action}
                                        </div>
                                        <div className={'cell' + (i + 1 === reportArr1.length ? ' bb0' : '')}>
                                            {rep.date}
                                        </div>
                                        <div className={'cell' + (i + 1 === reportArr1.length ? ' bb0' : '')}>
                                            {rep.result}
                                        </div>
                                        <div className={'cell br0 ' + (i + 1 === reportArr1.length ? ' bb0' : '')}>
                                            {rep.product}
                                        </div>
                                    </div>
                                </>
                            )}
                        </div>
                    </div>
                </div>

                <div className='blog__slider'>
                    <Swiper
                        onSwiper={setNavSwiper}
                        slidesPerView={1}
                        breakpoints={{
                            640: {
                                slidesPerView: 2,
                            },
                            // when window width is >= 768px
                            1024: {
                                slidesPerView: 3,
                            },
                        }}
                        spaceBetween={20}
                        autoplay={{
                            delay: 2500,
                            disableOnInteraction: false,
                        }}
                        modules={[Autoplay]}
                    >
                        {imgArr.map((el, i) =>
                            <SwiperSlide className='blog__slide relative'>
                                <Image
                                    src={el.img}
                                    layout='fill'
                                    alt='reports'
                                    objectFit='cover'
                                    loading='eager'
                                    placeholder="blur"
                                />
                                <div className='donor'>
                                    <Image
                                        src={donor}
                                        layout='fill'
                                        alt='donor'
                                        objectFit='cover'
                                        loading='eager'
                                        placeholder="blur"
                                    />
                                </div>
                            </SwiperSlide>
                        )}
                    </Swiper>
                </div>
            </div>
        </div>
    </Layout>
}

export default BackHome;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})