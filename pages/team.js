import { Layout } from "../components/Layout";
import Link from "next/link";
import Image from 'next/image';
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect} from "react";

import Sergey from '../public/images/team/Sergey.jpg';
import Bogdan from '../public/images/team/Bogdan.jpg';
import Nastya from '../public/images/team/Nastya.jpg';
import Oleksandr from '../public/images/team/Oleksandr.jpg';
import Tanya from '../public/images/team/Tanya.jpg';
import Taras from '../public/images/team/Taras.jpg';
import Maxim from '../public/images/team/Maxim.jpg';
import Yuriy from '../public/images/team/Yuriy.jpg';
import Natalia from '../public/images/team/Natalia.jpg';

const Team = () => {
    const { t } = useTranslation('common')
    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const arr = [
        {
            img: Sergey,
            name: t("main-page.team.Sergey"),
            // birth: '(10 червня 1980 року народження)'
            position: `${t("main-page.team.position.founder")}, ${t("main-page.team.position.director")}, ${t("main-page.team.position.ceo")}`
        },
        {
            img: Taras,
            name: t("main-page.team.Taras"),
            // birth: '(05 липня 1978 року народження)'
            position: `${t("main-page.team.position.founder")}, ${t("main-page.team.position.coordinator")}, ${t("main-page.team.position.ceo")}`,
            facebook: 'taras.vyazovchenko'
            // facebook: 'https://www.facebook.com/taras.vyazovchenko'
        },
        // {
        //     img: Valeriy,
        //     name: t("main-page.team.Valeriy"),
        //     // birth: '(05 липня 1978 року народження)'
        //     position: t("main-page.team.position.lawyer")
        // },
        {
            img: Maxim,
            name: t("main-page.team.Maxim"),
            position: t("main-page.team.position.estimate")
        },
        {
            img: Nastya,
            name: t("main-page.team.Nastya"),
            position: t("main-page.team.position.project")
        },
        {
            img: Oleksandr,
            name: t("main-page.team.Oleksandr"),
            position: t("main-page.team.position.project")
        },
        {
            img: Tanya,
            name: t("main-page.team.Tanya"),
            position: t("main-page.team.position.accountant")
        },
        {
            img: Natalia,
            name: t("main-page.team.Natalia"),
            position: t("main-page.team.position.psychological")
        },
        {
            img: Yuriy,
            name: t("main-page.team.Yyriy"),
            position: t("main-page.team.position.architecture")
        },
        {
            img: Bogdan,
            name: t("main-page.team.Bogdan"),
            position: t("main-page.team.position.volunteer")
        },
    ]

    const seo = {
        seo_title: t("seo.team.title"),
        seo_description: t("seo.team.description"),
        og_title: t("seo.team.title"),
        og_description: t("seo.team.description")
    }

    return <Layout seo={seo}>
        <div className='team section'>
            <div className="container">
                <h1 ref={borderRef}>{t("header.submenu.team")}</h1>

                <div className='team-wrap'>
                    {arr.map((el, i) => (
                        <div className='team-card' key={i}>
                            <div className='team-portrait'>
                                <Image
                                    src={el?.img}
                                    layout='fill'
                                    alt='portrait'
                                    objectFit='cover'
                                />
                            </div>

                            <h3>{el?.name}</h3>
                            <p>{el?.position}</p>

                            {el?.facebook &&
                                <a href={'https://www.facebook.com/' + el?.facebook}>
                                    {'facebook.com/' + el?.facebook}
                                </a>}
                        </div>
                    ))}
                </div>
            </div>
        </div>
    </Layout>

}

export default Team;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})