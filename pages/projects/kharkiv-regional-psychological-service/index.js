import {Layout} from "../../../components/Layout";
import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import Image from "next/image";

import style from './Project.module.scss';

import blog1 from "../../../public/images/blog/kharkiv-psychological/kharkiv-regional-psychological-service.png";
import donor1 from "../../../public/images/blog/kharkiv-psychological/donor1.png";
import donor2 from "../../../public/images/blog/kharkiv-psychological/donor2.png";
import donor3 from "../../../public/images/blog/kharkiv-psychological/donor3.png";
import map1 from "../../../public/images/blog/kharkiv-psychological/map1.jpg";

export default function Index() {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0.5,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("projects.Kharkiv-psychological-service.meta-title"),
        seo_description: t("projects.Kharkiv-psychological-service.meta-description"),
        og_title: t("projects.Kharkiv-psychological-service.meta-title"),
        og_description: t("projects.Kharkiv-psychological-service.meta-description"),
    }

    return <Layout seo={seo}>
        <div className={style.section + ' project-article section'}>
            <div className={style.title}>
                <div className={style.container}>
                    <h2>{t("projects.Kharkiv-psychological-service.title1")}</h2>
                    <h3>{t("projects.Kharkiv-psychological-service.title2")}</h3>
                    <h4 dangerouslySetInnerHTML={{
                        __html: t("projects.Kharkiv-psychological-service.subtitle")
                    }}/>
                </div>
            </div>


            <div className={style.green_text}>
                <div className={style.container}>
                    <h1 ref={borderRef}>{t("projects.Kharkiv-psychological-service.title")}</h1>
                    <p dangerouslySetInnerHTML={{
                        __html: t("projects.Kharkiv-psychological-service.text1")
                    }}/>

                    <ul>
                        <h3>{t("projects.Kharkiv-psychological-service.list1.title")}</h3>
                        <li>{t("projects.Kharkiv-psychological-service.list1.li1")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list1.li2")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list1.li3")}</li>
                    </ul>

                    <ul>
                        <h3>{t("projects.Kharkiv-psychological-service.list2.title")}</h3>
                        <li>{t("projects.Kharkiv-psychological-service.list2.li1")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list2.li2")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list2.li3")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list2.li4")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list2.li5")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list2.li6")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list2.li7")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list2.li8")}</li>
                    </ul>

                    <ul>
                        <h3>{t("projects.Kharkiv-psychological-service.list3.title")}</h3>
                        <li>{t("projects.Kharkiv-psychological-service.list3.li1")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list3.li2")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list3.li3")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list3.li4")}</li>
                    </ul>

                    <ul className='psychological-help-contact'>
                        <h2>{t("projects.Kharkiv-psychological-service.list4.title")}</h2>
                        <li>{t("projects.Kharkiv-psychological-service.list4.li1")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list4.li2")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list4.li3")}</li>
                        <li>{t("projects.Kharkiv-psychological-service.list4.li4")}</li>
                    </ul>

                    <p>{t("projects.Kharkiv-psychological-service.text2")}</p>
                </div>
                <svg className={style.wave} viewBox="0 0 625 95" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M625 0C501.8 67.2644 157 90.9962 0 94.454H625V0Z" fill="white"/>
                </svg>
            </div>

            <div className='donor-block'>
                <div className={style.container}>
                    <div className='image_map'>
                        <Image
                            src={map1}
                            layout='fill'
                            alt='blogHero'
                            objectFit='fill'
                            loading='eager'
                            placeholder="blur"
                        />
                    </div>

                    <div className='donor-block__image' >
                        <Image
                            src={blog1}
                            layout='fill'
                            alt='blogHero'
                            objectFit='contain'
                            loading='eager'
                            placeholder="blur"
                        />
                    </div>

                    <div className='donor-block__wrap'>
                        <div className={style.container}>
                            <div className='donor-block__item-wrap'>
                                <div className='donor-block__item'>
                                    <Image
                                        src={donor1}
                                        layout='fill'
                                        alt='blogHero'
                                        objectFit='fill'
                                        loading='eager'
                                        placeholder="blur"
                                    />
                                </div>
                                <div className='donor-block__item albom'>
                                    <Image
                                        src={donor3}
                                        layout='fill'
                                        alt='blogHero'
                                        objectFit='fill'
                                        loading='eager'
                                        placeholder="blur"
                                    />
                                </div>
                                <div className='donor-block__item'>
                                    <Image
                                        src={donor2}
                                        layout='fill'
                                        alt='blogHero'
                                        objectFit='fill'
                                        loading='eager'
                                        placeholder="blur"
                                    />
                                </div>
                            </div>

                            <p className={style.links} dangerouslySetInnerHTML={{
                                __html: t("projects.Kharkiv-psychological-service.text3")
                            }}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </Layout>
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})