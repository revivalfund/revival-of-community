import {Layout} from "../../components/Layout";
import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import Image from "next/image";

import img from "../../public/images/blog/first-three-month-stage-psychosocial-support/img1.jpg";

export default function Index() {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0.5,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("projects.Humanitarian-solidarity.meta-title"),
        seo_description: t("projects.Humanitarian-solidarity.meta-description"),
        og_title: t("projects.Humanitarian-solidarity.meta-title"),
        og_description: t("projects.Humanitarian-solidarity.meta-description"),
    }

    return <Layout seo={seo}>
        <div className="news-article section">
            <div className="container">
                <div className='news-article__image' ref={borderRef}>
                    <Image
                        src={img}
                        layout='fill'
                        alt='newsHero'
                        objectFit='cover'
                        loading='eager'
                        placeholder="blur"
                    />
                </div>

                <h1>{t("projects.Humanitarian-solidarity.title")}</h1>
                <p dangerouslySetInnerHTML={{
                    __html: t("projects.Humanitarian-solidarity.text1")
                }}/>
                <p dangerouslySetInnerHTML={{
                    __html: t("projects.Humanitarian-solidarity.text2")
                }}/>
            </div>
        </div>
    </Layout>
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})