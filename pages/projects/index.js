import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import BlogPreview from "../../components/Blog/BlogPreview";
import {Layout} from "../../components/Layout";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

import blog1 from "../../public/images/blog/kharkiv-psychological/kharkiv-regional-psychological-service.png";
import blog2 from "../../public/images/blog/first-three-month-stage-psychosocial-support/img1.jpg";

export default function ProjectsPage() {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: 'Проєкти БО «Відродження Громад»: допомога та підтримка відновлення України',
        seo_description: 'Змінюйте життя людей разом з нами. Дізнайтесь більше про наші ініціативи та досягнення',
        og_title: 'Проєкти БО «Відродження Громад»: допомога та підтримка відновлення України',
        og_description: 'Змінюйте життя людей разом з нами. Дізнайтесь більше про наші ініціативи та досягнення'
    }

    return <Layout seo={seo}>
        <div className='projects section'>
            <h1 ref={borderRef}>
                {t("projects.title")}
            </h1>

            <div className="container">
                <div className='projects__blog-wrap'>
                    <BlogPreview
                        slug={'/projects/kharkiv-regional-psychological-service'}
                        image={blog1}
                        title={t("projects.Kharkiv-psychological-service.title-preview")}
                        description={t("projects.Kharkiv-psychological-service.text-preview")}
                        date={t("projects.Kharkiv-psychological-service.date")}
                    />
                    <BlogPreview
                        slug={'/projects/humanitarian-solidarity'}
                        image={blog2}
                        title={t("projects.Humanitarian-solidarity.title-preview")}
                        description={t("projects.Humanitarian-solidarity.text-preview")}
                        date={t("projects.Humanitarian-solidarity.date")}
                    />
                </div>
            </div>
        </div>
    </Layout>
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})