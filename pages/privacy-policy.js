import { Layout } from "../components/Layout";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useInView } from "react-intersection-observer";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { useTranslation } from "next-i18next";

const Privacy = () => {
    const { t } = useTranslation('common')
    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    return <Layout>
        <div className='privacy section'>
            <div className="container">
                <div className='terms-wrap'>
                    <h1 ref={borderRef}>{t("privacy.title1")}</h1>

                    <p>{t("privacy.p1")}</p>

                    <h2>{t("privacy.title2")}</h2>
                    <p>{t("privacy.p2")}</p>

                    <h2>{t("privacy.title3")}</h2>
                    <p>{t("privacy.p3")} </p>

                    <h2>{t("privacy.title4")}</h2>
                    <p className='margin-bottom-small'>{t("privacy.p4-first")}</p>
                    <ul className='list'>
                        <li>{t("privacy.list.li1")}</li>
                        <li>{t("privacy.list.li2")}</li>
                        <li>{t("privacy.list.li3")}</li>
                        <li>{t("privacy.list.li4")}</li>
                        <li>{t("privacy.list.li5")}</li>
                        <li>{t("privacy.list.li6")}</li>
                    </ul>

                    <p>{t("privacy.p4-second")}</p>

                    <h2>{t("privacy.title5")}</h2>
                    <p>{t("privacy.p5")}</p>

                    <h2>{t("privacy.title6")}</h2>
                    <p>{t("privacy.p6")}</p>

                    <h2>{t("privacy.title7")}</h2>
                    <p>{t("privacy.p7-1")}<a href="https://uk.wikipedia.org/wiki/%D0%9A%D1%83%D0%BA%D0%B8" target='_blank' rel='nofollow'>https://uk.wikipedia.org</a>. {t("privacy.p7-1")}</p>

                    <h2>{t("privacy.title8")}</h2>
                    <p>{t("privacy.p8")}</p>

                    <h2>{t("privacy.title9")}</h2>
                    <p>{t("privacy.p9")}</p>

                    <h2>{t("privacy.title10")}</h2>
                    <p>{t("privacy.p10")}</p>

                    <h2>{t("privacy.title11")}</h2>
                    <p>{t("privacy.p11-1")}<a href="mailto:revivalfund2022@gmail.com">revivalfund2022@gmail.com</a>.{t("privacy.p11-2")}</p>
                </div>
            </div>
        </div>
    </Layout>

}

export default Privacy;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})