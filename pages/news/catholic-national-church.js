import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {Layout} from "../../components/Layout";
import Image from "next/image";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

import news2 from "../../public/images/news/catholic-national-church.jpg";


export default function Index() {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0.5,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("news.catholic-national-church.meta"),
        seo_description: t("news.catholic-national-church.description"),
        og_title: t("news.catholic-national-church.meta"),
        og_description: t("news.catholic-national-church.description"),
    }

    return <Layout seo={seo}>
        <div className="news-article section">
            <div className="container">
                <div className='news-article__image' ref={borderRef}>
                    <Image
                        src={news2}
                        layout='fill'
                        alt='newsHero'
                        objectFit='cover'
                        loading='eager'
                        placeholder="blur"
                    />
                </div>

                <h1>{t("news.catholic-national-church.title")}</h1>
                <p dangerouslySetInnerHTML={{
                    __html: t("news.catholic-national-church.text")
                }}/>
            </div>
        </div>
    </Layout>
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})