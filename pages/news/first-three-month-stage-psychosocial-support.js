import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect, useState} from "react";
import {Layout} from "../../components/Layout";
import Image from "next/image";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {Autoplay} from "swiper";
import {Swiper, SwiperSlide} from "swiper/react";
import "swiper/css";

import news1 from "../../public/images/news/first-three-month-stage-psychosocial-support/1.jpg";
import news2 from "../../public/images/news/first-three-month-stage-psychosocial-support/2.webp";
import news3 from "../../public/images/news/first-three-month-stage-psychosocial-support/3.webp";
import news4 from "../../public/images/news/first-three-month-stage-psychosocial-support/4.webp";



export default function Index() {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0.5,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("news.first-three-month-stage-psychosocial-support.meta"),
        seo_description: t("news.first-three-month-stage-psychosocial-support.description"),
        og_title: t("news.first-three-month-stage-psychosocial-support.meta"),
        og_description: t("news.first-three-month-stage-psychosocial-support.description"),
    }

    const [navSwiper, setNavSwiper] = useState(null);
    const imgArr = [{img: news2}, {img: news3}, {img: news4}]


    return <Layout seo={seo}>
        <div className="news-article section">
            <div className="container">
                <div className='news-article__image' ref={borderRef}>
                    <Image
                        src={news1}
                        layout='fill'
                        alt='newsHero'
                        objectFit='cover'
                        loading='eager'
                        placeholder="blur"
                        blurDataURL={news1}
                    />
                </div>

                <h1>{t("news.first-three-month-stage-psychosocial-support.title")}</h1>
                <p dangerouslySetInnerHTML={{
                    __html: t("news.first-three-month-stage-psychosocial-support.text")
                }}/>

                <Swiper
                    onSwiper={setNavSwiper}
                    slidesPerView={1}
                    breakpoints={{
                        640: {
                            slidesPerView: 2,
                        },
                        // when window width is >= 768px
                        1024: {
                            slidesPerView: 3,
                        },
                    }}
                    spaceBetween={20}
                    autoplay={{
                        delay: 2500,
                        disableOnInteraction: false,
                    }}
                    modules={[Autoplay]}
                >
                    {imgArr.map((el, i) =>
                        <SwiperSlide className='blog__slide relative' key={i}>
                            <Image
                                src={el.img}
                                layout='fill'
                                alt='reports'
                                objectFit='cover'
                                loading='eager'
                                placeholder="blur"
                            />
                        </SwiperSlide>
                    )}
                </Swiper>
            </div>
        </div>
    </Layout>
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})