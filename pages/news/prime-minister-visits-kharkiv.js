import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {Layout} from "../../components/Layout";
import Image from "next/image";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {Swiper, SwiperSlide} from "swiper/react";
import {Autoplay} from "swiper";
import {useState} from "react";

import news1 from "../../public/images/news/prime-minister-visits-kharkiv/1.jpg";
import news2 from "../../public/images/news/prime-minister-visits-kharkiv/2.jpg";
import news3 from "../../public/images/news/prime-minister-visits-kharkiv/3.jpg";
import news4 from "../../public/images/news/prime-minister-visits-kharkiv/4.jpg";
import news5 from "../../public/images/news/prime-minister-visits-kharkiv/5.jpg";
import news6 from "../../public/images/news/prime-minister-visits-kharkiv/6.jpg";
import news7 from "../../public/images/news/prime-minister-visits-kharkiv/7.jpg";

import "swiper/css";


export default function Index() {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0.5,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("news.prime-minister-visits-kharkiv.meta"),
        seo_description: t("news.prime-minister-visits-kharkiv.description"),
        og_title: t("news.prime-minister-visits-kharkiv.meta"),
        og_description: t("news.prime-minister-visits-kharkiv.description"),
    }

    const [navSwiper, setNavSwiper] = useState(null);
    const imgArr = [{img: news1}, {img: news2}, {img: news4}, {img: news5}, {img: news6}, {img: news7}]


    return <Layout seo={seo}>
        <div className="news-article section">
            <div className="container">
                <div className='news-article__image' ref={borderRef}>
                    <Image
                        src={news3}
                        layout='fill'
                        alt='newsHero'
                        objectFit='cover'
                        loading='eager'
                        placeholder="blur"
                    />
                </div>

                <h1>{t("news.prime-minister-visits-kharkiv.title")}</h1>
                <p dangerouslySetInnerHTML={{
                    __html: t("news.prime-minister-visits-kharkiv.text")
                }}/>

                <Swiper
                    onSwiper={setNavSwiper}
                    slidesPerView={1}
                    breakpoints={{
                        640: {
                            slidesPerView: 2,
                        },
                        // when window width is >= 768px
                        1024: {
                            slidesPerView: 3,
                        },
                    }}
                    spaceBetween={20}
                    autoplay={{
                        delay: 2500,
                        disableOnInteraction: false,
                    }}
                    modules={[Autoplay]}
                >
                    {imgArr.map((el, i) =>
                        <SwiperSlide className='blog__slide relative' key={i}>
                            <Image
                                src={el.img}
                                layout='fill'
                                alt='reports'
                                objectFit='cover'
                                loading='eager'
                                placeholder="blur"
                            />
                        </SwiperSlide>
                    )}
                </Swiper>
            </div>
        </div>
    </Layout>
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})