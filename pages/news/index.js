import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {Layout} from "../../components/Layout";
import {NewsCard} from "../../components/News/NewsCard";

import news1 from "../../public/images/news/roof-repair-rich-town.jpg";
import news2 from "../../public/images/news/catholic-national-church.jpg";
import news3 from "../../public/images/news/psychosocial-support-war-time/2.jpg";
import news4 from "../../public/images/news/prime-minister-visits-bucha/6.jpg";
import news5 from "../../public/images/news/prime-minister-visits-kharkiv/3.jpg";
import news6 from "../../public/images/news/how-are-you.webp";
import news7 from "../../public/images/news/first-three-month-stage-psychosocial-support/1.jpg";

const News = () => {
    const { t } = useTranslation('common')
    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("seo.news.title"),
        seo_description: t("seo.news.description"),
        og_title: t("seo.news.title"),
        og_description: t("seo.news.description")
    }

    return <Layout seo={seo}>
        <div className='news section'>
            <h1 ref={borderRef}>
                {t("news.title")}
            </h1>


            <div className="container">
                <div className='news__wrap'>
                    <NewsCard
                        slug={'/news/first-three-month-stage-psychosocial-support'}
                        image={news7}
                        title={t('news.first-three-month-stage-psychosocial-support.title')}
                        date={t('news.first-three-month-stage-psychosocial-support.date')}
                    />
                    <NewsCard
                        slug={'/news/how-are-you'}
                        image={news6}
                        title={t('news.prime-minister-visits-kharkiv.title')}
                        date={t('news.prime-minister-visits-kharkiv.date')}
                    />
                    <NewsCard
                        slug={'/news/prime-minister-visits-kharkiv'}
                        image={news5}
                        title={t('news.prime-minister-visits-kharkiv.title')}
                        date={t('news.prime-minister-visits-kharkiv.date')}
                    />
                    <NewsCard
                        slug={'/news/prime-minister-visits-bucha'}
                        image={news4}
                        title={t('news.prime-minister-visits-bucha.title')}
                        date={t('news.prime-minister-visits-bucha.date')}
                    />
                    <NewsCard
                        slug={'/news/psychosocial-support-war-time'}
                        image={news3}
                        title={t('news.psychosocial-support-war-time.title')}
                        date={t('news.psychosocial-support-war-time.date')}
                    />
                    <NewsCard
                        slug={'/news/catholic-national-church'}
                        image={news2}
                        title={t('news.catholic-national-church.title')}
                        date={t('news.catholic-national-church.date')}
                    />
                    <NewsCard
                        slug={'/news/roof-repair-rich-town'}
                        image={news1}
                        title={t('news.roof-repair-rich-town.title')}
                        date={t('news.roof-repair-rich-town.date')}
                    />
                </div>
            </div>
        </div>
    </Layout>
}

export default News;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})
