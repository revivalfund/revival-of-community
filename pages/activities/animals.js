import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {Layout} from "../../components/Layout";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import OncePay from "../../components/LiqPay/OncePay";
import Image from "next/image";
import {Swiper, SwiperSlide} from "swiper/react";
import {Autoplay} from "swiper";
import {useState} from "react";

import img1 from "../../public/images/animals/1.jpg";
import img2 from "../../public/images/animals/2.jpg";
import img3 from "../../public/images/animals/3.jpg";
import img4 from "../../public/images/animals/4.jpeg";
import img5 from "../../public/images/animals/5.jpg";
import img6 from "../../public/images/animals/6.jpg";
import img7 from "../../public/images/animals/7.jpg";

import "swiper/css";


const Animals = () => {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })
    const [navSwiper, setNavSwiper] = useState(null);

    const imgArr = [{img: img1}, {img: img2}, {img: img3}, {img: img4}, {img: img5},
        // {img: img6}, {img: img7}
    ]

    const seo = {
        seo_title: t("seo.animals.title"),
        seo_description: t("seo.animals.description"),
        og_title: t("seo.animals.title"),
        og_description: t("seo.animals.description")
    }


    return <Layout seo={seo}>
        <div className='activities section'>
            <div className="container">
                <h1 ref={borderRef}>
                    {t("activities-page.animal.title")}

                    <div className='activities-btn'>
                        <OncePay
                            title={t("header.want-help")}
                            description={t("activities-page.animal.title")}
                        />
                    </div>
                </h1>
                <p>{t("activities-page.animal.text1")}</p>
                <p>{t("activities-page.animal.text2")}</p>
            </div>

            <div className='activities_wrap animals-wrap'>
                <div className='activities_item'>
                    <h3 style={{textAlign: "center", marginBottom: "1rem"}}>{t("activities-page.animal.title")}</h3>

                    <div className='activities_item-image'>
                        <Swiper
                            onSwiper={setNavSwiper}
                            speed={600}
                            slidesPerView={3}
                            grabCursor={true}
                            spaceBetween={20}
                            autoplay={{
                                delay: 2500,
                                disableOnInteraction: false,
                            }}
                            modules={[Autoplay]}
                            className='activities_item-swiper'
                        >
                            {/*<>*/}
                            {/*    {imgArr?.map((el, i) =>*/}
                            {/*        <SwiperSlide key={i} className='activities_item-slide'>*/}
                            {/*            <Image*/}
                            {/*                src={el?.img}*/}
                            {/*                layout='fill'*/}
                            {/*                alt='animals'*/}
                            {/*                objectFit='cover'*/}
                            {/*            />*/}
                            {/*        </SwiperSlide>*/}
                            {/*    )}*/}
                            {/*</>*/}

                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img1} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img2} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img3} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img4} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img5} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img6} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img7} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                        </Swiper>
                    </div>

                    <p className='activities_last-text'>{t("activities-page.last-text")}</p>
                </div>
            </div>
        </div>
    </Layout>
}

export default Animals;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})