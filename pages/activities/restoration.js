import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect, useState} from "react";
import {Layout} from "../../components/Layout";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {Swiper, SwiperSlide} from "swiper/react";
import {Autoplay} from "swiper";
import Image from "next/image";
import {useRouter} from "next/router";
import OncePay from "../../components/LiqPay/OncePay";

import img0 from "../../public/images/restoration/restoration.jpg";
import img1 from "../../public/images/restoration/1.jpg";
import img2 from "../../public/images/restoration/2.jpg";
import img3 from "../../public/images/restoration/3.jpg";
import img4 from "../../public/images/restoration/4.jpg";
import img5 from "../../public/images/restoration/5.jpg";
import img6 from "../../public/images/restoration/6.jpg";
import img7 from "../../public/images/restoration/7.jpg";
import img8 from "../../public/images/restoration/8.jpg";
import img9 from "../../public/images/restoration/9.jpg";
import img10 from "../../public/images/restoration/10.jpg";
import img11 from "../../public/images/restoration/11.jpg";
import img12 from "../../public/images/restoration/12.jpg";
import img13 from "../../public/images/restoration/13.jpg";
import img14 from "../../public/images/restoration/14.jpg";
import img15 from "../../public/images/restoration/15.jpg";

import "swiper/css";


const Restoration = () => {
    const [navSwiper, setNavSwiper] = useState(null);

    const { t } = useTranslation('common')
    const router = useRouter();

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const actsArrUA = [
        {
            name: 'Акт пошкодження вул. Західна 2',
            url: 'https://drive.google.com/file/d/1_q9gL4Ac5oDg0B-By6Pl7sqJt0v2gjeU/preview'
        },
        {
            name: 'Акт пошкодження вул. Західна 4',
            url: 'https://drive.google.com/file/d/1F7erW63XZZAp_usc3xvEu1LwartUXIRl/preview'
        },
        {
            name: 'Акт пошкодження вул. Західна 6',
            url: 'https://drive.google.com/file/d/14Y3KB-zs24Z0SmNPDvpAkjSo0UzkpHrI/preview'
        },
        {
            name: 'Акт пошкодження вул. Західна 8',
            url: 'https://drive.google.com/file/d/1SLXG-EEySrWRqOXThOns_JXSd-UmpjBP/preview'
        },
        {
            name: 'Акт пошкодження вул. Західна 10',
            url: 'https://drive.google.com/file/d/1mLkzWYoXc0uekJsTQIqqun6NY5Q9UkC4/preview'
        },
        {
            name: 'Акт пошкодження вул. Мечнікова 68-6',
            url: 'https://drive.google.com/file/d/1ObRmPnCXgp2ipuTHUOyE3FszO6zrXF3H/preview'
        },
    ]
    const actsArrEU = [
        {
            name: 'Act of damage Zakhidna str. 2',
            url: 'https://drive.google.com/file/d/1FnaSPucZhPR855urs5jOvVg3Lai4U_tq/preview'
        },
        {
            name: 'Act of damage Zakhidna str. 4',
            url: 'https://drive.google.com/file/d/1em3voJvFK2FlAfR7Mq8Uy537MtyUVLLI/preview'
        },
        {
            name: 'Act of damage Zakhidna str. 6',
            url: 'https://drive.google.com/file/d/1o0V8CMxel3p3H4EUm1_6eWqn8NmX-Pqk/preview'
        },
        {
            name: 'Act of damage Zakhidna str. 8',
            url: 'https://drive.google.com/file/d/1pYtcEhtHIjUJAicwr6DwI1vvd_Dvpgjk/preview'
        },
        {
            name: 'Act of damage Zakhidna str. 10',
            url: 'https://drive.google.com/file/d/1GaeWSPCeRSaNBR3DbWaBuOINUPRSBt1e/preview'
        },
        {
            name: 'Act of damage Mechnikova str. 68-6',
            url: 'https://drive.google.com/file/d/1W44u8ocJrgliJzvxnscvstxYrm69Mk3N/preview'
        },
    ]
    const restorationArr = [{img: img0}, {img: img1}, {img: img2}, {img: img3},
        {img: img4}, {img: img5}, {img: img6}, {img: img7},
        {img: img8}, {img: img9}, {img: img10}, {img: img11},
        {img: img12}, {img: img13}, {img: img14}, {img: img15},
        // {img: img16}, {img: img17}, {img: img18}, {img: img19},
        // {img: img20}, {img: img21}, {img: img22}, {img: img23}
    ]

    const [documentActive, setDocumentActive] = useState('')

    const documentToggle = (act) => {
        if (documentActive === act) {
            setDocumentActive('')
        } else {
            setDocumentActive(act)
        }
    }

    const seo = {
        seo_title: t("seo.restoration.title"),
        seo_description: t("seo.restoration.description"),
        og_title: t("seo.restoration.title"),
        og_description: t("seo.restoration.description")
    }

    return <Layout seo={seo}>
        <div className='activities section'>
            <div className="container">
                <h1 ref={borderRef}>
                    {t("activities-page.restoration.title")}

                    <div className='activities-btn'>
                        <OncePay
                            title={t("header.want-help")}
                            description={t("activities-page.restoration.title")}
                        />
                    </div>
                </h1>
                <p>{t("activities-page.restoration.text1")}</p>
                <p>{t("activities-page.restoration.text2")}</p>
                <p>{t("activities-page.restoration.text3")}</p>
                <p>{t("activities-page.restoration.text4")}</p>
            </div>

            <div className='activities_wrap'>
                <div className='activities_item'>
                    <div className='container'>
                        <div className='activities_item-content'>
                            <h2>{t("activities-page.restoration.title2")}</h2>
                            <p className='activities_item-text-under'>
                                {t("activities-page.restoration.text-under")}
                            </p>
                            <div className='activities_item-wrap'>
                                <div className='activities_item-documents'>
                                    <h3>{t("activities-page.restoration.title3")}</h3>

                                    <ul>
                                        {(router?.locale === 'ua' ? actsArrUA : actsArrEU)?.map((el, i) =>
                                            <li key={i} onClick={() => documentToggle(el?.name)} className={documentActive === el?.name ? 'active' : ''}>
                                                <span>{el?.name}</span>
                                                <iframe src={el?.url} allow="autoplay"/>
                                            </li>
                                        )}
                                    </ul>
                                    <p>
                                        {t("activities-page.restoration.text5-1")} <br/>
                                        {t("activities-page.restoration.text5-2")}
                                    </p>
                                </div>
                            </div>

                            <h3>{t("activities-page.restoration.title-img")}</h3>
                        </div>
                    </div>
                    <div className='activities_item-image'>
                        <Swiper
                            onSwiper={setNavSwiper}
                            speed={600}
                            slidesPerView={3}
                            grabCursor={true}
                            spaceBetween={20}
                            autoplay={{
                                delay: 2500,
                                disableOnInteraction: false,
                            }}
                            modules={[Autoplay]}
                            className='activities_item-swiper'
                        >
                            {/*<>*/}
                            {/*    {restorationArr?.map((el, i) =>*/}
                            {/*        <SwiperSlide key={i} className='activities_item-slide'>*/}
                            {/*            <Image*/}
                            {/*                src={el?.img}*/}
                            {/*                layout='fill'*/}
                            {/*                alt='animals'*/}
                            {/*                objectFit='cover'*/}
                            {/*            />*/}
                            {/*        </SwiperSlide>*/}
                            {/*    )}*/}
                            {/*</>*/}
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img0} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img1} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img2} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img3} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img3} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img4} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img5} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img6} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img7} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img8} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img9} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img10} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img11} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img12} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img13} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img14} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                            <SwiperSlide  className='activities_item-slide'>
                                <Image src={img15} layout='fill' alt='animals' objectFit='cover'/>
                            </SwiperSlide>
                        </Swiper>
                    </div>

                    <p className='activities_last-text'>{t("activities-page.last-text")}</p>
                </div>
            </div>
        </div>
    </Layout>
}

export default Restoration;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})