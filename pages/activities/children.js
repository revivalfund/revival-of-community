import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {Layout} from "../../components/Layout";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import Link from "next/link";
import Image from "next/image";

import img1 from '../../public/images/children/1.jpg';
import img2 from '../../public/images/children/2.jpg';
import img3 from '../../public/images/children/3.jpg';
import OncePay from "../../components/LiqPay/OncePay";

const Children = () => {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("seo.children.title"),
        seo_description: t("seo.children.description"),
        og_title: t("seo.children.title"),
        og_description: t("seo.children.description")
    }

    return <Layout seo={seo}>
        <div className='activities section'>
            <div className="container">
                <h1 ref={borderRef}>
                    {t("activities-page.children.title")}

                    <div className='activities-btn'>
                        <OncePay
                            title={t("header.want-help")}
                            description={t("activities-page.children.title")}
                        />
                    </div>
                </h1>
                <p>{t("activities-page.children.text1")}</p>
                <p>{t("activities-page.children.text2")}</p>
                <p>{t("activities-page.children.text3")}</p>
            </div>

            <div className='activities_wrap'>
                <div className='activities_item'>
                    <div className='container'>
                        <div className='activities_item-content'>
                            <div className='activities_item-list list'>
                                <ul className='image-grid grid-3'>
                                    <li>
                                        <h3>{t("activities-page.children.list.li1")}</h3>

                                        <Link href=''>
                                            <a>
                                                <div className='image-grid__item'>
                                                    <Image
                                                        src={img3}
                                                        layout='fill'
                                                        alt='children'
                                                        objectFit='cover'
                                                    />
                                                </div>
                                            </a>
                                        </Link>
                                    </li>

                                    <li>
                                        <h3>{t("activities-page.children.list.li2")}</h3>

                                        <Link href=''>
                                            <a>
                                                <div className='image-grid__item'>
                                                    <Image
                                                        src={img1}
                                                        layout='fill'
                                                        alt='children'
                                                        objectFit='cover'
                                                    />
                                                </div>
                                            </a>
                                        </Link>
                                    </li>

                                    <li>
                                        <h3>{t("activities-page.children.list.li3")}</h3>

                                        <Link href=''>
                                            <a>
                                                <div className='image-grid__item'>
                                                    <Image
                                                        src={img2}
                                                        layout='fill'
                                                        alt='children'
                                                        objectFit='cover'
                                                    />
                                                </div>
                                            </a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <p className='activities_last-text'>{t("activities-page.last-text")}</p>
                    </div>
                </div>
            </div>
        </div>
    </Layout>
}

export default Children;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})