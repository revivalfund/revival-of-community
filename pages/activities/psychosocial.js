import {useTranslation} from "next-i18next";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {Layout} from "../../components/Layout";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import OncePay from "../../components/LiqPay/OncePay";

import "swiper/css";
import {Autoplay} from "swiper";
import {Swiper, SwiperSlide} from "swiper/react";
import Image from "next/image";
import {useState} from "react";

import news1 from "../../public/images/psychosocial/1.JPG";
import news2 from "../../public/images/psychosocial/2.JPG";
import news3 from "../../public/images/psychosocial/3.JPG";

import "swiper/css";

const Psychosocial = () => {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("seo.psychosocial.title"),
        seo_description: t("seo.psychosocial.description"),
        og_title: t("seo.psychosocial.title"),
        og_description: t("seo.psychosocial.description")
    }

    const [navSwiper, setNavSwiper] = useState(null);
    const imgArr = [{img: news1}, {img: news2}, {img: news3}]


    return <Layout seo={seo}>
        <div className='activities section'>
            <div className="container">
                <h1 ref={borderRef}>
                    {t("activities-page.psychosocial.title")}

                    <div className='activities-btn'>
                        <OncePay
                            title={t("header.want-help")}
                            description={t("activities-page.animal.title")}
                        />
                    </div>
                </h1>
                <p>{t("activities-page.psychosocial.text1")} <br/> {t("activities-page.psychosocial.text2")}</p>

                <Swiper
                    onSwiper={setNavSwiper}
                    slidesPerView={1}
                    breakpoints={{
                        640: {
                            slidesPerView: 2,
                        },
                        // when window width is >= 768px
                        1024: {
                            slidesPerView: 3,
                        },
                    }}
                    spaceBetween={20}
                    autoplay={{
                        delay: 2500,
                        disableOnInteraction: false,
                    }}
                    modules={[Autoplay]}
                >
                    {imgArr.map((el, i) =>
                        <SwiperSlide className='blog__slide relative' key={i}>
                            <Image
                                src={el.img}
                                layout='fill'
                                alt='reports'
                                objectFit='cover'
                                loading='eager'
                                placeholder="blur"
                                blurDataURL={el.img}
                            />
                        </SwiperSlide>
                    )}
                </Swiper>
            </div>
        </div>
    </Layout>
}

export default Psychosocial;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})