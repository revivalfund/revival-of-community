import { Layout } from "../../components/Layout";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import { useEffect } from "react";
import {useTranslation} from "next-i18next";
import {useRouter} from "next/router";

const FundDocuments = () => {
    const { t } = useTranslation('common')
    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })
    const router = useRouter();

    const seo = {
        seo_title: t("seo.documents.title"),
        seo_description: t("seo.documents.description"),
        og_title: t("seo.documents.title"),
        og_description: t("seo.documents.description")
    }

    return <Layout seo={seo}>
        <div className='section documents'>
            <div className="container">
                <h1 ref={borderRef}>{t("documents-page.h1")}</h1>

                <div className='documents_wrap'>
                    <div className='documents_item'>
                        <h3>{t("documents-page.register")}</h3>
                        <iframe src="https://drive.google.com/file/d/1WoYXIsZ3y8jkvDN_NlaxymWOs1K1Gk3X/preview" allow="autoplay"/>
                    </div>

                    <div className='documents_item'>
                        <h3>{t("documents-page.extract")}</h3>
                        <iframe src="https://drive.google.com/file/d/1k8GpDgLB1yXzcpTXb6HIsniqbkq3zRxe/preview" allow="autoplay"/>
                    </div>

                    <div className='documents_item'>
                        <h3>{t("documents-page.regulations")}</h3>
                        <iframe src="https://drive.google.com/file/d/1GOj87l-itcQe0qkI8_7ZH7Mn5msGGvih/preview" allow="autoplay"/>
                    </div>

                    <div className='documents_item'>
                        {router.locale === 'ua' ? <>
                            <h3>
                                БЛАГОДІЙНА ОРГАНІЗАЦІЯ «ВІДРОДЖЕННЯ ГРОМАД»
                            </h3>
                            <p>
                                Ідентифікаційний код: 44770808
                            </p>
                            <p>
                                Місцезнаходження : Україна, 04159, місто Київ, вул. Кульженків Сім'ї, будинок 33, оф.
                                303
                            </p>
                            <p>
                                IBAN: UA90 3052 9900 0002 6006 0162 3191 2
                                в АТ КБ «ПРИВАТБАНК» у м. Києві
                            </p>
                            <p>
                                Рішення про неприбутковість: № 2226544600105 від 24.05.2022 року
                            </p>
                        </> : <>
                            <h3>
                                CHARITY ORGANIZATION "COMMUNITIES REVIVAL"
                            </h3>
                            <p>
                                Identification code: 44770808
                            </p>
                            <p>
                                Location: Ukraine, 04159, Kyiv city, str. Kulzhenkiv family, building 33, office 303
                            </p>
                            <p>
                                IBAN: UA90 3052 9900 0002 6006 0162 3191 2
                                in JSC CB "PRIVATBANK" in Kyiv
                            </p>
                            <p>
                                Decision on non-profitability: No. 2226544600105 dated 24/05/2022
                            </p>
                        </>}
                    </div>

                    <div className='documents_item'>
                        <h3 id='offer'>{t("documents-page.offer")}</h3>

                        {router.locale === 'ua'
                            ? <iframe src="https://drive.google.com/file/d/1oQf86_oMNIS6mNwop7P_DD0P1AWKIiIE/preview"
                                      allow="autoplay"/>
                            : <iframe src="https://drive.google.com/file/d/1muxS-mUaaw5GV7-CWEbCV8vImXl_hKaO/preview"
                                      allow="autoplay"/>
                        }
                    </div>
                </div>
            </div>
        </div>
    </Layout>
}

export default FundDocuments;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})