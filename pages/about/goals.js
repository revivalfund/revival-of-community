import { Layout } from "../../components/Layout";
import Cork from "../../components/Cork";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useEffect } from "react";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useTranslation} from "next-i18next";

const Partners = () => {
    const { t } = useTranslation('common')
    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    return <Layout>
        <div className='section'>
            <h1 ref={borderRef}>
                Our Goals
            </h1>

            <Cork />
        </div>
    </Layout>

}

export default Partners;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})