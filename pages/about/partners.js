import { Layout } from "../../components/Layout";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useEffect } from "react";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import {useTranslation} from "next-i18next";
import {Partner} from "../../components/Partner";

import un from "../../public/images/partners/un.png";
import pact from "../../public/images/partners/pact.png";
import howAreYou from "../../public/images/partners/howAreYou.png";
import israaid from "../../public/images/partners/israaid.png";
import academy from "../../public/images/partners/academy.png";
import minReint from "../../public/images/partners/minReint.png";
import resilientUkraine from "../../public/images/partners/resilientUkraine.png";
import chemonics from "../../public/images/partners/chemonics.png";
import moz from "../../public/images/partners/moz.png";
import bezbar from "../../public/images/partners/bezbar.png";
import ocss from "../../public/images/partners/ocss.png";
import revival from "../../public/images/partners/revival.png";
import care from "../../public/images/partners/care.png";
import swissTPH from "../../public/images/partners/swissTPH.png";
import osc from "../../public/images/partners/osc.png";
import eipgz from "../../public/images/partners/eipgz.png";
import temple from "../../public/images/partners/temple.png";
import publicHealth from "../../public/images/partners/publicHealth.png";
import philadelphia from "../../public/images/partners/philadelphia.png";
import coalition from "../../public/images/partners/coalition.png";
import homeGames from "../../public/images/partners/homeGames.png";
import playrix from "../../public/images/partners/playrix.png";


const Partners = () => {
    const { t } = useTranslation('common')
    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("seo.partners.title"),
        seo_description: t("seo.partners.description"),
        og_title: t("seo.partners.title"),
        og_description: t("seo.partners.description")
    }

    const partners = [
        {
            img: un,
            type: '',
            title: t("partners-page.partners.un"),
            projects: [
                {
                    text: t("partners-page.projects.un"),
                    link: ''
                }
            ]
        },
        {
            img: pact,
            type: 'square',
            title: t("partners-page.partners.pact"),
            projects: [
                {
                    text: t("partners-page.projects.knp"),
                    link: ''
                }
            ]
        },
        {
            img: howAreYou,
            type: '',
            title: t("partners-page.partners.howAreYou"),
            projects: [
                {
                    text: t("partners-page.projects.kharkiv"),
                    link: '/projects/kharkiv-regional-psychological-service'
                },
                {
                    text: t("partners-page.projects.knp"),
                    link: ''
                },
                {
                    text: t("partners-page.projects.duringWar"),
                    link: '/news/first-three-month-stage-psychosocial-support'
                }
            ]
        },
        {
            img: israaid,
            type: '',
            title: t("partners-page.partners.israaid"),
            projects: [
                {
                    text: t("partners-page.projects.duringWar"),
                    link: '/news/first-three-month-stage-psychosocial-support'
                }
            ]
        },
        {
            img: academy,
            type: 'square',
            title: t("partners-page.partners.academy"),
            projects: [
                {
                    text: t("partners-page.projects.kharkiv"),
                    link: '/projects/kharkiv-regional-psychological-service'
                },
                {
                    text: t("partners-page.projects.knp"),
                    link: ''
                },
                {
                    text: t("partners-page.projects.duringWar"),
                    link: '/news/first-three-month-stage-psychosocial-support'
                }
            ]
        },
        {
            img: minReint,
            type: '',
            title: t("partners-page.partners.minReint"),
            projects: [
                {
                    text: t("partners-page.projects.kharkiv"),
                    link: '/projects/kharkiv-regional-psychological-service'
                }
            ]
        },
        {
            img: resilientUkraine,
            type: '',
            title: t("partners-page.partners.resilientUkraine"),
            projects: [
                {
                    text: t("partners-page.projects.kharkiv"),
                    link: '/projects/kharkiv-regional-psychological-service'
                }
            ]
        },
        {
            img: chemonics,
            type: '',
            title: t("partners-page.partners.chemonics"),
            projects: [
                {
                    text: t("partners-page.projects.kharkiv"),
                    link: '/projects/kharkiv-regional-psychological-service'
                }
            ]
        },
        {
            img: moz,
            type: '',
            title: t("partners-page.partners.moz"),
            projects: [
                {
                    text: t("partners-page.projects.duringWar"),
                    link: '/news/first-three-month-stage-psychosocial-support'
                }
            ]
        },
        {
            img: bezbar,
            type: 'square',
            title: t("partners-page.partners.bezbar"),
            projects: [
                {
                    text: t("partners-page.projects.kharkiv"),
                    link: '/projects/kharkiv-regional-psychological-service'
                },
                {
                    text: t("partners-page.projects.duringWar"),
                    link: '/news/first-three-month-stage-psychosocial-support'
                }
            ]
        },
        {
            img: ocss,
            type: '',
            title: t("partners-page.partners.ocss"),
            projects: [
                {
                    text: t("partners-page.projects.kharkiv"),
                    link: '/projects/kharkiv-regional-psychological-service'
                }
            ]
        },
        {
            img: revival,
            type: '',
            title: t("partners-page.partners.revival"),
            projects: [
                {
                    text: t("partners-page.projects.backHome"),
                    link: '/projects/humanitarian-solidarity'
                }
            ]
        },
        {
            img: care,
            type: '',
            title: t("partners-page.partners.care"),
            projects: [
                {
                    text: t("partners-page.projects.backHome"),
                    link: '/projects/humanitarian-solidarity'
                }
            ]
        },
        {
            img: swissTPH,
            type: '',
            title: t("partners-page.partners.swissTPH"),
            projects: [
                {
                    text: t("partners-page.projects.knp"),
                    link: ''
                }
            ]
        },
        {
            img: osc,
            type: '',
            title: t("partners-page.partners.osc"),
            projects: [
                {
                    text: t("partners-page.projects.knp"),
                    link: ''
                }
            ]
        },
        {
            img: eipgz,
            type: '',
            title: t("partners-page.partners.eipgz"),
            projects: [
                {
                    text: t("partners-page.projects.knp"),
                    link: ''
                }
            ]
        },
        {
            img: temple,
            type: '',
            title: t("partners-page.partners.temple"),
            projects: [
                {
                    text: t("partners-page.projects.knp"),
                    link: ''
                }
            ]
        },
        {
            img: publicHealth,
            type: '',
            title: t("partners-page.partners.publicHealth"),
            projects: [
                {
                    text: t("partners-page.projects.knp"),
                    link: ''
                }
            ]
        },
        {
            img: philadelphia,
            type: '',
            title: t("partners-page.partners.philadelphia"),
            projects: [
                {
                    text: t("partners-page.projects.knp"),
                    link: ''
                }
            ]
        },
        {
            img: coalition,
            type: '',
            title: t("partners-page.partners.coalition"),
            projects: [
                {
                    text: t("partners-page.projects.knp"),
                    link: ''
                }
            ]
        },
        {
            img: homeGames,
            type: '',
            title: t("partners-page.partners.homeGames"),
            projects: [
                {
                    text: t("partners-page.projects.donor"),
                    link: '/blog/home-games'
                }
            ]
        },
        {
            img: playrix,
            type: '',
            title: '',
            projects: [
                {
                    text: t("partners-page.projects.donor"),
                    link: ''
                }
            ]
        },
    ]

    return <Layout seo={seo}>
        <div className='section partners'>
            <div className="container">
                <h1 ref={borderRef}>
                    {t("partners-page.h1")}
                </h1>

                <div className='partners_block'>
                    {partners.map(partner =>
                        <Partner
                            img={partner.img}
                            type={partner.type}
                            title={partner.title}
                            projects={partner.projects}
                            key={partner.title}
                        />
                    )}
                </div>
            </div>
        </div>
    </Layout>

}

export default Partners;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})