import { Layout } from "../../components/Layout";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import {useInView} from "react-intersection-observer";
import {useDispatch} from "react-redux";
import { useEffect } from "react";
import {useTranslation} from "next-i18next";
import {Swiper, SwiperSlide} from "swiper/react";
import {Autoplay} from "swiper";
import Image from "next/image";
import BlogPreview from "../../components/Blog/BlogPreview";
import BlogSlider from "../../components/Blog/BlogSlider";

import "swiper/css";

import img1 from "../../public/images/reports/1.jpg";
import img2 from "../../public/images/reports/2.jpg";
import img3 from "../../public/images/reports/3.jpg";
import img4 from "../../public/images/reports/4.jpg";
import img5 from "../../public/images/reports/5.jpg";
import img6 from "../../public/images/reports/6.jpg";
import img7 from "../../public/images/reports/7.jpg";
import img8 from "../../public/images/reports/8.jpg";
import img9 from "../../public/images/reports/9.jpg";
import img10 from "../../public/images/reports/10.jpg";
import img11 from "../../public/images/reports/11.jpg";
import img12 from "../../public/images/reports/12.jpg";
import img13 from "../../public/images/reports/13.jpg";

import blog1 from "../../public/images/blog/how-to-help-Ukrain/how-to-help-Ukrain.jpg";
import blog2 from "../../public/images/blog/home-games/title.jpg";
import blog3 from "../../public/images/blog/back-home/title.jpg";
import common from "../../public/images/blog/report-for-2022/report-for-2022.png";
import blog5 from "../../public/images/blog/psychological-assistance/img1.png";


const Report = () => {
    const { t } = useTranslation('common')

    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("seo.report.title"),
        seo_description: t("seo.report.description"),
        og_title: t("seo.report.title"),
        og_description: t("seo.report.description")
    }

    const imgArr = [{img: img1}, {img: img2}, {img: img3}, {img: img4}, {img: img5},
        {img: img6}, {img: img7}, {img: img8}, {img: img9}, {img: img10}, {img: img11}, {img: img12}, {img: img13}
    ]

    return <Layout seo={seo}>
        <div className='report section'>
            <h1 ref={borderRef}>
                {t("reports-page.h1")}
            </h1>

            <div className="container">
                <p>
                    <br/>
                    {t("reports-page.p")}
                </p>

                <BlogSlider images={imgArr}/>

                <div className='report__blog-wrap'>
                    <BlogPreview
                        slug={'/blog/organizing-psychological-assistance'}
                        image={blog5}
                        title={t('blog.psychological-assistance.title')}
                        description={t('blog.psychological-assistance.text-preview')}
                        date={t('blog.psychological-assistance.date')}
                    />
                    <BlogPreview
                        slug={'/blog/restoration-in-gorenka'}
                        image={common}
                        title={t('blog.restoration-in-gorenka.title')}
                        description={t('blog.restoration-in-gorenka.text-preview')}
                        date={t('blog.restoration-in-gorenka.date')}
                    />
                    <BlogPreview
                        slug={'/blog/reporting-for-2022'}
                        image={common}
                        title={t('blog.reporting-for-2022.title')}
                        description={t('blog.reporting-for-2022.text-preview')}
                        date={t('blog.reporting-for-2022.date')}
                    />
                    <BlogPreview
                        slug={'/blog/come-back-home'}
                        image={blog3}
                        title={t('blog.back-home.title-preview')}
                        description={t('blog.back-home.text-preview')}
                        date={t('blog.back-home.date')}
                    />
                    <BlogPreview
                        slug={'/blog/home-games'}
                        image={blog2}
                        title={t('blog.home-games.title-preview')}
                        description={t('blog.home-games.text-preview')}
                        date={t('blog.home-games.date')}
                    />
                    {/*<BlogPreview*/}
                    {/*    slug={'/blog/how-to-help-Ukraine'}*/}
                    {/*    image={blog1}*/}
                    {/*    title={t('blog.how-to-help-Ukraine.title')}*/}
                    {/*    description={t('blog.how-to-help-Ukraine.description')}*/}
                    {/*    date={t('blog.how-to-help-Ukraine.date')}*/}
                    {/*/>*/}
                </div>
            </div>
        </div>
    </Layout>

}

export default Report;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})



