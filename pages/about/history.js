import { Layout } from "../../components/Layout";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useInView } from "react-intersection-observer";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { useTranslation } from "next-i18next";
import se from "react-datepicker";

const History = () => {
    const { t } = useTranslation('common')
    const {ref: borderRef, inView: isBorderVisible} = useInView({
        threshold: 0,
    })
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch({
            type: 'BORDER_VISIBLE',
            border: isBorderVisible,
        })
    })

    const seo = {
        seo_title: t("seo.history.title"),
        seo_description: t("seo.history.description"),
        og_title: t("seo.history.title"),
        og_description: t("seo.history.description")
    }

    return <Layout seo={seo}>
        <div className='history section'>
            <div className="container">
                <h1
                    className='history-title'
                    ref={borderRef}
                >
                    {t("history-page.h1")}
                </h1>

                <p>
                    <b>{t("history-page.text.p1-begin")}</b> {t("history-page.text.p1")}
                </p>

                <p>{t("history-page.text.p2")}</p>

                <p>{t("history-page.text.p3")}</p>

                <p>{t("history-page.text.p4-start")} <b>{t("history-page.text.p4-middle")}</b> {t("history-page.text.p4-end")}</p>

                <p>
                    {/*<b>{t("history-page.text.p5-begin")}</b> */}
                    {t("history-page.text.p5")}
                </p>

                <p id='goals'><b>{t("history-page.text.p6-begin")}</b> {t("history-page.text.p6")}</p>

                <div className='list'>
                    <h2>
                        <b>{t("history-page.list.title")}</b>
                    </h2>

                    <ul>
                        <li>{t("history-page.list.li1")}</li>
                        <li>{t("history-page.list.li2")}</li>
                        <li>{t("history-page.list.li3")}</li>
                        <li>{t("history-page.list.li4")}</li>
                        <li>{t("history-page.list.li5")}</li>
                        <li>{t("history-page.list.li6")}</li>
                        <li>{t("history-page.list.li7")}</li>
                    </ul>
                </div>

                <div className='history_cards'>
                    <p>
                        <b>{t("history-page.cards.title")}</b>
                    </p>

                    <div className='history_cards_wrap'>
                        <div className='item first'>{t("history-page.cards.card1")}</div>
                        <div className='item second'>{t("history-page.cards.card2")}</div>
                        <div className='item third'>{t("history-page.cards.card3")}</div>
                    </div>
                </div>
            </div>
        </div>
    </Layout>

}

export default History;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['common']),
    },
})
