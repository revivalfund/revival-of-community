/** @type {import('next').NextConfig} */
const { i18n } = require('./next-i18next.config');

const nextConfig = {
  reactStrictMode: true,
  i18n,
  async redirects() {
    return [
      {
        source: '/about',
        destination: '/about/history',
        permanent: true,
      },
    ]
  },
}

module.exports = nextConfig
